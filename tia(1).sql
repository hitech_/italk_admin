-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 31, 2017 at 03:53 PM
-- Server version: 5.5.57-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tia`
--

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(100) NOT NULL,
  `module_display_name` varchar(100) NOT NULL,
  `parent_module_name` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `sequence` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `module_name`, `module_display_name`, `parent_module_name`, `icon`, `sequence`) VALUES
(1, 'tag', 'Secondary Tag', 'Master', 'fa fa-tasks', 1),
(2, 'designation', 'Designation', 'Master', 'fa fa-tasks', 2),
(3, 'category', 'Primary Tag', 'Master', 'fa fa-tasks', 3),
(4, 'media', 'Media Type', 'Master', 'fa fa-tasks', 4),
(5, 'model', 'Model', 'Portfolio', 'zmdi zmdi-collection-text', 5),
(6, 'value', 'Value', 'Portfolio', 'zmdi zmdi-collection-text', 6),
(7, 'portfolio', 'Portfolio', 'Portfolio', 'zmdi zmdi-collection-text', 7),
(8, 'portfolio_team', 'Team', 'Portfolio', 'zmdi zmdi-collection-text', 8),
(9, 'new_thinking', 'New Thinking', 'New Thinking', 'zmdi zmdi-invert-colors', 9),
(10, 'team', 'Team', 'Team', 'fa fa-users', 10),
(11, 'career', 'Career Listing', 'Career', 'zmdi zmdi-graduation-cap', 11),
(12, 'intern', 'Intern', 'Career', 'zmdi zmdi-graduation-cap', 12),
(13, 'homepage', 'Homepage', 'Homepage', 'fa fa-home', 13),
(14, 'newsletter', 'Newsletter', 'newsletter', 'fa fa-newspaper-o', 14),
(15, 'resume', 'Resume', 'Resume', 'fa fa-paperclip', 15),
(16, 'footer', 'Footer', 'Footer', 'fa fa-list', 16);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(20) NOT NULL,
  `article` varchar(1000) NOT NULL,
  `image_url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `category`, `article`, `image_url`) VALUES
(1, 'Category3', 'asddasd', NULL),
(2, 'Category3', 'asddasd', 'ams_notify.png'),
(3, 'Category3', 'asddasd', 'news/'),
(4, 'Category3', 'asddasd', 'news/ams_notify.png');

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE IF NOT EXISTS `news_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `news_comments`
--

CREATE TABLE IF NOT EXISTS `news_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `user_id` int(20) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `news_comments`
--

INSERT INTO `news_comments` (`id`, `news_id`, `user_id`, `comment`, `status`) VALUES
(1, 1, 1, 'updated1', 1),
(2, 1, 1, 'adasd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `party_details`
--

CREATE TABLE IF NOT EXISTS `party_details` (
  `id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receive_payment`
--

CREATE TABLE IF NOT EXISTS `receive_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_date` date NOT NULL,
  `party_name` varchar(50) NOT NULL,
  `dill_date` date NOT NULL,
  `bill_no` int(11) NOT NULL,
  `pay_amount` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=124 ;

--
-- Dumping data for table `receive_payment`
--

INSERT INTO `receive_payment` (`id`, `payment_date`, `party_name`, `dill_date`, `bill_no`, `pay_amount`) VALUES
(123, '2017-10-10', 'AAA', '2017-10-09', 123, '3000');

-- --------------------------------------------------------

--
-- Table structure for table `recieve_payment`
--

CREATE TABLE IF NOT EXISTS `recieve_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_date` date NOT NULL,
  `party_name` varchar(50) NOT NULL,
  `bill_date` date NOT NULL,
  `bill_no` int(11) NOT NULL,
  `pay_amount` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=124 ;

--
-- Dumping data for table `recieve_payment`
--

INSERT INTO `recieve_payment` (`id`, `payment_date`, `party_name`, `bill_date`, `bill_no`, `pay_amount`) VALUES
(123, '2017-10-10', 'AAA', '2017-10-09', 123, '3000');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order`
--

CREATE TABLE IF NOT EXISTS `sales_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_no` varchar(10) NOT NULL,
  `select_date` varchar(12) NOT NULL,
  `party_name` varchar(50) NOT NULL,
  `party_email_id` varchar(50) NOT NULL,
  `place` varchar(20) NOT NULL,
  `agent_name` varchar(20) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `seller_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `seller_id` (`seller_id`),
  KEY `seller_id_2` (`seller_id`),
  KEY `seller_id_3` (`seller_id`),
  KEY `seller_id_4` (`seller_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sales_order`
--

INSERT INTO `sales_order` (`id`, `bill_no`, `select_date`, `party_name`, `party_email_id`, `place`, `agent_name`, `amount`, `seller_id`) VALUES
(1, 'AA123', '2017-10-10', 'axis', 'axis@axis.com', 'ghansoli', 'AA', '500.00', 2),
(2, 'AA144', '2017-10-03', 'a12', 'a12@aa.com', 'mumbai', 'aa', '5000.00', 2),
(4, 'AA124', '2018-10-10', 'Party1', 'Party@gmail.com', 'Thane', 'Agent1', '3000', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `mobile_no` bigint(20) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `last_login` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0' COMMENT '1 for admin,2 for seller,3 for agent',
  `user_type` tinyint(4) NOT NULL DEFAULT '0',
  `forgot_password_token` varchar(50) NOT NULL,
  `forgot_password_token_expire` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `mobile_no`, `email_id`, `password`, `status`, `last_login`, `ip_address`, `role`, `user_type`, `forgot_password_token`, `forgot_password_token_expire`, `created_at`, `updated_at`, `updated_by`) VALUES
(1, 'Admin', '', 1234567890, 'admin@tia.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, '2017-10-13 11:16:32', '120.63.178.6', 0, 1, '', '0000-00-00 00:00:00', '2017-10-13 05:46:32', '2017-05-17 23:16:17', 0),
(2, 'karina', 'jhangiani', 9999999999, 'karina@lightbox.vc', '5031056d6ed6c5d836d226a98f306d36', 0, '2017-07-11 08:48:33', '115.110.127.250', 2, 0, '', '0000-00-00 00:00:00', '2017-10-18 08:56:33', '2017-06-13 04:10:01', 0),
(3, 'tina ', 'mehta', 0, 's1@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, '2017-06-04 22:02:45', '115.110.127.250', 2, 2, '', '0000-00-00 00:00:00', '2017-10-18 04:11:19', '0000-00-00 00:00:00', 0),
(4, 'har', 'test', 9638527410, 'har@box.com', 'a257a91001c2575f1989948a5c923e3c', 1, '0000-00-00 00:00:00', '120.63.177.247', 0, 0, '', '0000-00-00 00:00:00', '2017-06-08 00:40:39', '2017-06-08 00:10:39', 0),
(5, 'First Name', 'Last Name', 7894561230, 'har1@box.com', 'a257a91001c2575f1989948a5c923e3c', 1, '0000-00-00 00:00:00', '120.63.177.247', 0, 0, '', '0000-00-00 00:00:00', '2017-10-12 12:08:00', '0000-00-00 00:00:00', 0),
(7, 'Ronil', 'Chauhan', 0, 'ronil.chauhan@please-see.com', '94395de65611b91dbcbe8af036e29ed2', 1, '0000-00-00 00:00:00', '125.99.78.114', 0, 0, '', '0000-00-00 00:00:00', '2017-10-12 12:08:05', '2017-06-19 07:42:11', 0),
(8, 'varun', 'verma ', 0, 'varun@lightbox.vc', '5031056d6ed6c5d836d226a98f306d36', 1, '2017-07-28 03:53:31', '115.110.127.250', 0, 0, '', '0000-00-00 00:00:00', '2017-07-27 22:23:31', '2017-07-26 06:17:07', 0),
(9, 'Avinash', 'Thadani', 0, 'avithadani@gmail.com', '8d99cc01a7e90f3977fb27a4352c61c9', 1, '2017-07-27 13:57:50', '49.33.179.214', 0, 0, '', '0000-00-00 00:00:00', '2017-07-27 08:27:50', '0000-00-00 00:00:00', 0),
(11, 'Nasreen', 'S', 0, 'nasreen@ascratech.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, '2017-08-02 09:52:23', '120.63.178.6', 0, 0, '808301597f2f6f412c3', '2017-07-31 14:24:01', '2017-08-02 04:22:23', '0000-00-00 00:00:00', 0),
(12, 'Test', 'T', 0, 'T@g.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, '0000-00-00 00:00:00', '114.79.134.26', 0, 0, '', '0000-00-00 00:00:00', '2017-10-18 06:13:26', '0000-00-00 00:00:00', 0),
(13, 'T1', 'T2', 0, 't1@g.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, '0000-00-00 00:00:00', '120.63.178.6', 0, 0, '', '0000-00-00 00:00:00', '2017-10-07 03:11:47', '0000-00-00 00:00:00', 0),
(14, 'Khushbu ', 'Kotecha', 0, 'khushbu@ascratech.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, '2017-08-01 07:32:22', '120.63.177.247', 0, 0, '', '0000-00-00 00:00:00', '2017-10-12 12:09:25', '2017-08-01 07:41:49', 0),
(15, 'Sid', 'Athan', 0, 'sid.athan@please-see.com', 'e807f1fcf82d132f9bb018ca6738a19f', 1, '2017-08-02 12:00:04', '27.106.94.167', 0, 0, '', '0000-00-00 00:00:00', '2017-10-07 03:12:09', '2017-08-09 01:43:41', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_module`
--

CREATE TABLE IF NOT EXISTS `user_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `user_module`
--

INSERT INTO `user_module` (`id`, `user_id`, `module_id`) VALUES
(1, 3, 9),
(2, 3, 10),
(14, 2, 4),
(13, 2, 3),
(12, 2, 2),
(11, 2, 1),
(15, 2, 5),
(16, 2, 6),
(17, 2, 7),
(18, 2, 8),
(19, 2, 9),
(20, 2, 10),
(21, 2, 11),
(22, 2, 12),
(23, 2, 13),
(25, 7, 9),
(26, 7, 10),
(78, 9, 16),
(77, 9, 15),
(76, 9, 14),
(75, 9, 13),
(74, 9, 12),
(73, 9, 11),
(72, 9, 10),
(71, 9, 9),
(70, 9, 8),
(69, 9, 7),
(68, 9, 6),
(67, 9, 5),
(66, 9, 4),
(65, 9, 3),
(64, 9, 2),
(63, 9, 1),
(79, 11, 9),
(103, 16, 5),
(82, 14, 7),
(83, 14, 8),
(84, 14, 9),
(85, 14, 10),
(86, 14, 12),
(87, 15, 1),
(88, 15, 2),
(89, 15, 3),
(90, 15, 4),
(91, 15, 5),
(92, 15, 6),
(93, 15, 7),
(94, 15, 8),
(95, 15, 9),
(96, 15, 10),
(97, 15, 11),
(98, 15, 12),
(99, 15, 13),
(100, 15, 14),
(101, 15, 15),
(102, 15, 16),
(104, 16, 6),
(105, 16, 7),
(106, 16, 8),
(107, 16, 9),
(108, 16, 10),
(109, 16, 11),
(110, 16, 12),
(111, 16, 13);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD CONSTRAINT `foreign_key` FOREIGN KEY (`seller_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
