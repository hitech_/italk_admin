<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['login'] = array(
    array(
        'field' => 'username',
        'label' => 'Username',
        'rules' => 'trim|required|verifyCredentials'
    ),
    array(
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'trim|required|md5'
    )
);
$config['forgot_password'] = array(
    array(
        'field' => 'username',
        'label' => 'Email Id',
        'rules' => 'trim|required|valid_email|verifyCredentials'
    )
);
$config['reset_password'] = array(
    array(
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'confirm_password',
        'label' => 'Password',
        'rules' => 'trim|required|matches[password]'
    )
);
$config['sales_order'] =array(
    array(
        'field' => 'bill_no',
        'label' => 'bill no',
        'rules' => 'trim|required'
    ), 
    array(
        'field' => 'bill_date',
        'label' => 'Date',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'party_name',
        'label' => 'Party name',
        'rules' => 'trim|required'
    ), 
    array(
        'field' => 'party_email_id',
        'label' => 'Email id',
        'rules' => 'trim|required|valid_email'
    ), 
    array(
        'field' => 'place',
        'label' => 'place',
        'rules' => 'trim|required'
    ), 
    array(
        'field' => 'agent_name',
        'label' => 'Agent Name',
        'rules' => 'trim|required'
    ), 
    array(
        'field' => 'amount',
        'label' => 'amount',
        'rules' => 'trim|required|numeric'
    ) 
);
$config['news'] = array(
    array(
        'field' => 'category',
        'label' => 'category',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'article',
        'label' => 'Article ',
        'rules' => 'trim|required'
    )
);

$config['comments'] = array(
    array(
        'field' => 'comment',
        'label' => 'category',
        'rules' => 'trim|required'
    )
);

$config['users'] = array(
    array(
        'field' => 'first_name',
        'label' => 'First Name',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'last_name',
        'label' => 'Last Name ',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'email_id',
        'label' => 'Email',
        'rules' => 'trim|required|valid_email'
    )
);