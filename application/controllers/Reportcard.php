<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportcard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        ini_set("display_errors", 1);
        //$this->output->enable_profiler(TRUE);

        //load model
        $this->load->model(array('do_logs','categories_model'));

        // load Pagination library
        //$this->load->library('pagination');
    }

    public function index(){

        $user_id = $this->input->get('users');

        $data['pagetitle'] = 'Report Card';
        $data['filter'] = $this->input->get();

        //get users
        $order_by = ['u_username' => 'ASC'];
        $users = $this->do_logs->get_user_detail([], $order_by);
        if(!empty($users)) {
            $keys = array_column($users, 'u_user_id');
            $users = array_combine($keys, $users);
        }
        $data['users'] = $users;
        //echo '<pre>'; print_r($data); die;

        //get categories
        $data['category_wise_cnt'] = !empty($user_id) ? $this->categories_model->get_category_wise_item_count() : [];
        //echo '<pre>'; print_r($data['category_cnt']); exit;

        $log_report = !empty($user_id) ? $this->do_logs->get_reports($user_id) : [];
        if(!empty($log_report)) {
            $keys = array_column($log_report, 'category_id');
            $log_report = array_combine($keys, $log_report);
        }
        $data['log_report'] = $log_report;
        //echo '<pre>'; print_r($data); exit;

        $this->load->view('includes/header',$data);
        $this->load->view('reportcard/index', $data);
        $this->load->view('includes/footer');

    }

    public function exportExcelReportCard() {
        $user_id = $this->input->get('users');
        $data = [];

        //get users
        if(!empty($user_id)) {
            $users = $this->do_logs->get_user_detail(['user_id'=>$user_id], []);
            $data['users'] = $users;
            //echo '<pre>'; print_r($data); die;

            //get categories
            $data['category_wise_cnt'] = !empty($user_id) ? $this->categories_model->get_category_wise_item_count() : [];
            //echo '<pre>'; print_r($data['category_cnt']); exit;

            $log_report = !empty($user_id) ? $this->do_logs->get_reports($user_id) : [];
            if(!empty($log_report)) {
                $keys = array_column($log_report, 'category_id');
                $log_report = array_combine($keys, $log_report);
            }
            $data['log_report'] = $log_report;
            //echo '<pre>'; print_r($data); exit;
        }
        $this->do_logs->exportCSVReportCard($data);

    }
}