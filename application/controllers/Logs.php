<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller {
	public function __construct() {
        parent::__construct();
        ini_set("display_errors", 1);
        //$this->output->enable_profiler(TRUE);

        //load model
        $this->load->model(array('do_logs'));

        // load Pagination library
        $this->load->library('pagination');
    }
    public function index(){
        $filter = $this->input->get();
        $data['pagetitle']='Logs';

        if(!empty($filter)) {
            $param = $filter;
        } else {
            //default filter
            $param = [
                'disabilitytypetext' => 'Normal',
                'disabilitytype' => 1,
                'duration' => 1,
                'durationtext' => 'Daily'
            ];
        }

        $query_string   = !empty($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING']: http_build_query($param);
        $data['query_string'] = '?'.$query_string;

    	$data['param'] = $param;

        // load config file
        $this->config->load('pagination', TRUE);
        $settings = $this->config->item('pagination');
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;
        //$settings['per_page'] = 2;
        $limit = $settings['per_page'];
        $offset = ($limit * $start_index) - $limit;
        $total_records = $this->do_logs->get_total_logs($param);

        $settings['total_rows'] = $total_records;
        $settings['base_url'] = base_url() . 'index.php/logs/index';
        $this->pagination->initialize($settings);

        // build paging links
        $data["links"] = $this->pagination->create_links();

        //get logs
        $order_by = ['L.created_at' => 'DESC'];
        $data['logs'] = $this->do_logs->logs($param, $limit, $offset, $order_by);

        //echo '<pre>'; print_r($data); die;

    	$this->load->view('includes/header',$data);
        $this->load->view('log/index', $data);
        $this->load->view('includes/footer');
    }

    public function view(){
        $filter = $this->input->get();
        $data['filter'] = $filter;

        $query_string   = isset($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING']:'';
        $data['query_string'] = $query_string;

        // load config file
        $this->config->load('pagination', TRUE);
        $settings = $this->config->item('pagination');
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        //$settings['per_page'] = 2;
        $limit = $settings['per_page'];
        $offset = ($limit * $start_index) - $limit;
        $total_records = $this->do_logs->get_total_userlogs($filter);

        $settings['total_rows'] = $total_records;
        $settings['base_url'] = base_url() . 'index.php/logs/view';
        $this->pagination->initialize($settings);

        //offset
        $data['offset'] = $offset;

        //echo '<pre>'; print_r($total_records); exit;

        // build paging links
        $data["links"] = $this->pagination->create_links();

        //get logs
        $order_by = ['l.created_at' => 'DESC'];
        $data['logs'] = $this->do_logs->get_userlogs($filter, $limit, $offset, $order_by);
        //echo '<pre>'; print_r($data['logs']); exit;

        //get user detail
        $data['user_detail'] = $this->do_logs->get_user_detail($filter);
        //echo '<pre>'; print_r($data['user_detail']); exit;

        $this->load->view('includes/header');
        $this->load->view('log/view',$data);
        $this->load->view('includes/footer');
   }

   public function emaillogs(){
       //$html = 'This is karthik kumar';

//       $data = [
//           'data' => 1
//       ];
//       $this->load->library('pdf');
//       $this->pdf->load_view('log/pdfreport', $data);
//       $this->pdf->render();
//       $this->pdf->stream('log/pdfreport', $data);


//       $pdfpath = 'myspdf';
//       $this->load->library('pdf');
//       $pdf = $this->pdf->load();
//       $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822));
//       $pdf->logo = file_get_contents(site_url('assets/img/logo.png'));
//       $pdf->WriteHTML($html); // write the HTML into the PDF
//       $pdf->Output('mypdf', 'F'); // save to file because we can

//       echo 'done';
//exit;

       $emailsubject="Your new password for italk";
       $emailbody='Hello karthik kumar';
       $this->email->from('italk@example.com', 'Italk');
       $this->email->to('karthikkumar@ymail.com');
       $this->email->subject('This is a subject');
       $this->email->message($emailbody);
       $buffer = file_get_contents('http://hi-techpeople.com/italk/index.php/createpdf');
       $this->email->attach($buffer, 'attachment', 'report.pdf', 'application/pdf');
       if($this->email->send()){
            echo 'test';exit;

       }
       echo 'ddd'; exit;
//        $param['user_id']=$user_id;
//
//        $data['logs']=$this->do_logs->userdetails($param);
//        $this->load->view('includes/header');
//        $this->load->view('log/view',$data);
//        $this->load->view('includes/footer');
    }
}