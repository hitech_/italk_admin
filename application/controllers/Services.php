<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {
	public function __construct() {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET,OPTIONS,POST,DELETE");
        //header('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        //$this->load->language('english/form_validation_lang');
        $this->load->model(array('Services_model', 'auth_model'));
        $this->date = !empty($_GET['updated_at']) ? $_GET['updated_at'] : '';
        $this->date = ($this->date == "null") ? "" : $this->date;
        $this->track_id = (!empty($_GET['track_id']) && is_numeric($_GET['track_id'])) ? $_GET['track_id'] : 0;
        $this->page_no = !empty($_GET['page_no']) ? $_GET['page_no'] : 1;
        $this->limit = !empty($_GET['limit']) ? $_GET['limit'] : 50;

    }

    public function get_total_categories() {
        $count = $this->Services_model->get_total_categories();
        $data = [
            'count' => $count
        ];
        echo json_encode($data);
    }

    public function get_categories(){
        $limit = $this->limit;
        $offset = ($this->page_no -1) * $limit;
        $result = $this->Services_model->get_categories($limit, $offset);
    	echo json_encode($result);
    }

    public function get_total_subcategories() {
        $count = $this->Services_model->get_total_subcategories();
        $data = [
          'count' => $count
        ];
        echo json_encode($data);
    }

    public function get_subcategories(){
        $limit = $this->limit;
        $offset = ($this->page_no -1) * $limit;
        $result = $this->Services_model->get_subcategories($limit, $offset);
        echo json_encode($result);
    }

    /*
    public function get_deleted_list(){
        $result = $this->Services_model->get_deleted_list();
        echo json_encode($result);
    }
    */

    public function get_total_sentences() {
        $count = $this->Services_model->get_total_sentences();
        $data = [
            'count' => $count
        ];
        echo json_encode($data);
    }

    public function get_sentences(){
        $limit = $this->limit;
        $offset = ($this->page_no -1) * $limit;
        $result = $this->Services_model->get_sentences($limit, $offset);
        echo json_encode($result);
    }

    public function get_words(){
        $result = $this->Services_model->get_words();
        echo json_encode($result);
    }

    public function registeruser(){
        $data=array();
        if($this->input->post()){
            //print_r($this->input->post()); die;
            $validationerrors=array();
            $finalvalidationarray=array();
            $this->form_validation->set_rules('u_username', 'Username', 'required|is_unique[users.u_username]', array('required'=>'username:The Username field is required.', 'is_unique'=>'username:Username already exists.'));
            $this->form_validation->set_rules('u_password', 'Password', 'required', array('required'=>'password:The Password field is required.'));
            $this->form_validation->set_rules('u_email_id', 'Email id', 'required|valid_email|is_unique[users.u_email_id]|callback_check_parent_email', array('required'=>'emailid:The Email id field is required.', 'valid_email'=>'emailid:The Email id field must be valid email.', 'is_unique'=>'emailid: email id already exists.'));

            $_POST['u_mobileno']=str_replace('+', '', $_POST['u_mobileno']);

            $this->form_validation->set_rules('u_mobileno', 'Mobile no', 'required|numeric|callback_check_mobile_unique|callback_check_mobile_num', array('required'=>'mobile: The Mobile no field is required.'));

            $this->form_validation->set_rules('u_gender', 'Gender', 'required', array('required'=>'gender:The Gender field is required.'));
            $this->form_validation->set_rules('u_disability_type', 'Disability Type', 'required', array('required'=>'disability:The Disability type field is required.'));
            $this->form_validation->set_rules('u_parent_email_id', 'Parent Email id', 'required|valid_email|callback_check_email', array('required'=>'parentemailid: The Parent Email id field is required.', 'valid_email'=>'parentemailid: The Parent email id must be valid email.'));
            if($this->form_validation->run()==false){
                $validationerrors=explode('.', rtrim(strip_tags(str_replace("\n", '', validation_errors())), "."));
                foreach($validationerrors as $val){
                    $explodevalid=explode(':', $val);
                    $valid[$explodevalid[0]]=$explodevalid[1];
                }
                $data['status']=false;
                $data['error']['validation_errors']=$valid;
            }else{
                $insertparam['u_username']=$this->input->post('u_username');
                $insertparam['u_password']=md5($this->input->post('u_password'));
                $insertparam['u_email_id']=$this->input->post('u_email_id');
                $insertparam['u_mobileno']=str_replace('+', '', $this->input->post('u_mobileno'));
                $insertparam['u_landline']=$this->input->post('u_landline');
                $insertparam['u_gender']=$this->input->post('u_gender');
                $insertparam['u_disability_type']=$this->input->post('u_disability_type');
                $insertparam['u_parent_email_id']=$this->input->post('u_parent_email_id');
                $insertparam['u_terms_and_conditions']=$this->input->post('u_terms_and_conditions');
                $insertparam['u_added_date']=date('Y-m-d H:i:s');
                $insertparam['u_updated_date']=date('Y-m-d H:i:s');
                $res=$this->auth_model->registeruser($insertparam);
                if(is_array($res)){
                    $data['status']=false;
                    $data['error']='User already exists in database';
                }else if(is_bool($res)){
                    $data['status']=true;
                }else{
                    $data['status']=false;
                    $data['error']='Error occurred while adding data';
                }
            }
        }else{
           $data['status']=false;
           $data['error']='No parameter received';
        }
        echo json_encode($data);
    }

    public function check_parent_email($str){
        $param['u_email_id']=$str;
        $results=$this->auth_model->check_parent_email($param);
        if($results > 0){
            $this->form_validation->set_message('check_parent_email', 'emailid: Email id already register as parent email id.');
            return false;
        }else{
            return true;
        }
    }

    public function check_email($str){
        $param['u_parent_email_id']=$str;
        $results=$this->auth_model->check_email($param);
        if($results > 0){
            $this->form_validation->set_message('check_email', 'parentemailid: Email id already register as user email id.');
            return false;
        }else{
            return true;
        }
    }

    public function check_mobile_unique($str){
        $param['mobile_no']=str_replace('+', '', $str);
        $results=$this->auth_model->check_mobile_unique($param);
        if($results > 0){
            $this->form_validation->set_message('check_mobile_unique', 'mobile: Mobile no already exists.');
            return false;
        }else{
            return true;
        }
    }

    public function check_mobile_num($str){
        $mobileno=str_replace('+', '', $str);
        if(!is_numeric($mobileno)){
            $this->form_validation->set_message('check_mobile_num', 'mobile: Mobile no should be numeric only.');
            return false;
        }else{
            return true;
        }
    }

    public function auth(){
        $data=array();
        if($this->input->post()){
            $validationerrors=array();
            $finalvalidationarray=array();
            $this->form_validation->set_rules('u_email_id', 'Email id', 'required', array('required'=>'emailid:The Email/Username field is required.'));
            $this->form_validation->set_rules('u_password', 'Password', 'required', array('required'=>'password:The Password field is required.'));
            if($this->form_validation->run()==false){
                $validationerrors=explode('.', rtrim(strip_tags(str_replace("\n", '', validation_errors())), "."));
                foreach($validationerrors as $val){
                    $explodevalid=explode(':', $val);
                    $valid[$explodevalid[0]]=$explodevalid[1];
                }
                $data['status']=false;
                $data['error']['validation_errors']=$valid;
            }else{
                $param['u_email_id']=$this->input->post('u_email_id');
                $param['u_password']=$this->input->post('u_password');
                $data=$this->auth_model->auth($param);
            }
        }else{
            $data['status']=false;
            $data['error']='No parameters received';
        }
        echo json_encode($data);
    }

    public function forgotpassword(){
        $data=array();
        if($this->input->post()){
            $validationerrors=array();
            $finalvalidationarray=array();
            $this->form_validation->set_rules('u_email_id', 'Email id', 'required|valid_email', array('required'=>'emailid:The Email id field is required.', 'valid_email'=>'emailid: The Email id must be valid email'));
            if($this->form_validation->run()==false){
                $validationerrors=explode('.', rtrim(strip_tags(str_replace("\n", '', validation_errors())), "."));
                foreach($validationerrors as $val){
                    $explodevalid=explode(':', $val);
                    $valid[$explodevalid[0]]=$explodevalid[1];
                }
                $data['status']=false;
                $data['error']['validation_errors']=$valid;
            }else{
                $param['u_email_id']=$this->input->post('u_email_id');
                $param['forgotpassword']=true;
                $data=$this->auth_model->auth($param);
            }
        }else{
            $data['status']=false;
            $data['error']='No parameters received';
        }
        echo json_encode($data);
    }

    public function updateprofile(){
        if($this->input->post()){
            $validationerrors=array();
            $finalvalidationarray=array();
            $this->form_validation->set_rules('u_email_id', 'Email id', 'callback_check_email_in_both');
            $this->form_validation->set_rules('u_mobileno', 'Mobile No', 'callback_check_mobile_unique');
            if($this->form_validation->run()==true){
                $param['u_user_id']=$this->input->post('u_user_id');
                $param['u_email_id']=$this->input->post('u_email_id');
                $param['u_mobileno']=$this->input->post('u_mobileno');
                $param['u_landline']=$this->input->post('u_landline');

                $res=$this->auth_model->updateprofile($param);
                if($res){
                    $data['status']=true;
                    $data['result']=$this->get_by_id($param['u_user_id']);
                }else{
                    $data['status']=false;
                    $data['error']='Error occurred while updating data';
                }
            }else{
                $validationerrors=explode('.', rtrim(strip_tags(str_replace("\n", '', validation_errors())), "."));
                foreach($validationerrors as $val){
                    $explodevalid=explode(':', $val);
                    $valid[$explodevalid[0]]=$explodevalid[1];
                }
                $data['status']=false;
                $data['error']['validation_errors']=$valid;
            }
        }else{
            $data['status']=false;
            $data['error']='No parameters received';
        }
        echo json_encode($data);
    }

    public function changepassword(){
        if($this->input->post()){
            $validationerrors=array();
            $finalvalidationarray=array();
            $this->form_validation->set_rules('u_existing_password', 'Existing Password', 'required', array('required'=>'existingpassword: Existing Password field is required.'));
            $this->form_validation->set_rules('u_new_password', 'New Password', 'required', array('required'=>'newpassword: New password field is required.'));
            $this->form_validation->set_rules('u_confirm_password', 'Confirm Password', 'required|matches[u_new_password]', array('required'=>'confirmpassword: Confirm password field is required.', 'matched'=>'New password and comfirm password should be the same.'));
            if($this->form_validation->run()==true){
                $param['u_user_id']=$this->input->post('u_user_id');
                $param['u_existing_password']=$this->input->post('u_existing_password');
                $param['u_new_password']=$this->input->post('u_new_password');
                $param['u_confirm_password']=$this->input->post('u_confirm_password');
                $res=$this->auth_model->changepassword($param);
                if($res){
                    $data['status']=true;
                }else{
                    $data['status']=false;
                    $data['error']['validation_errors']['existingpassword']='Existing password does not macth.';
                }
            }else{
                $validationerrors=explode('.', rtrim(strip_tags(str_replace("\n", '', validation_errors())), "."));
                foreach($validationerrors as $val){
                    $explodevalid=explode(':', $val);
                    $valid[$explodevalid[0]]=$explodevalid[1];
                }
                $data['status']=false;
                $data['error']['validation_errors']=$valid;
            }
        }else{
            $data['status']=false;
            $data['error']='No parameters received';
        }
        echo json_encode($data);
    }

    public function check_email_in_both($str){
        $param['emailid']=$str;
        $results=$this->auth_model->check_email_in_both($param);
        if($results > 0){
            $this->form_validation->set_message('check_email_in_both', 'emailid: Email id already exists.');
            return false;
        }else{
            return true;
        }
    }

    public function check_existing_password($str){
        $param['u_password']=$str;
        $results=$this->auth_model->check_existing_password($param);
        if(!$results){
            $this->form_validation->set_message('check_existing_password', 'existingpassword: Existing password does not macth with our database.');
            return false;
        }
    }

    private function get_by_id($id){
        $this->db->from('users');
        $this->db->where('u_user_id',$id);
        return $this->db->get()->row_array();
    }

    public function addlog(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if(!empty($postdata)){
            $insert_array = [];
            $current_datetime = date('Y-m-d H:i:s');
            foreach ($postdata as $k => $v) {
                unset($v['id']);
                $v['created_at'] = $current_datetime;
                $insert_array[$k] = $v;
            }

            $res = $this->db->insert_batch('logs',$insert_array);
            $data['status']=true;
        }else{
            $data['status']=false;
            $data['error']='No parameters received';
        }
        echo json_encode($data);
    }

    private function remove_key($array,$key){
        foreach ($array as $a_key => $value) {
            unset($array[$a_key][$key]);
        }
        return $array;
    }
    public function update_logout_time(){
        $data['status']=false;
        $postdata = json_decode(file_get_contents("php://input"), true);
        if(!empty($postdata)){
            //$this->db->set('last_logout_time',$postdata['logout_time']);
            $this->db->set('last_logout_time', date('Y-m-d H:i:s'));
            $this->db->where('u_user_id',$postdata['u_user_id']);
            $res = $this->db->update('users');
            if($res) {
                $data['status']=true;
            }
        }
        echo json_encode($data);
    }
}