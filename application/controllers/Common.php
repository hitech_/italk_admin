<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {
	public function __construct() {
        header("Content-type: text/html; charset=UTF-8");
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET,OPTIONS,POST,DELETE");
        /*header('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');*/
        parent::__construct();
        $this->load->model('do_common');
    }

    public function delete(){
        if($this->input->post()){
            $param['datetime'] = date('Y-m-d H:i:s');
            $param['table_name'] = $this->input->post('table_name');
            $param['id'] = $this->input->post('id');

            $res=$this->do_common->delete($param);
            if($res){
                $data['error']=false;
                $data['message']='Deleted Successfully';
                $data['data']=true;
            }else{
                $data['error']=true;
                $data['message']='Error occurred while deleting data';
                $data['data']=false;
            }
        } else {
            $data['error']=true;
            $data['message']='No parameter';
            $data['data']=false;
        }
        echo json_encode($data);

        /*
        if($this->input->post()){
            $param['created_date']=date('Y-m-d H:i:s');
            $param['module']=$this->input->post('module');
            $param['id']=$this->input->post('id');
            if($param['prefix']){
                $param['prefix']=$this->input->post('prefix');
            }
            
            $res=$this->do_common->delete($param);
            if($res){
                $data['error']=false;
                $data['message']='Deleted Successfully';
                $data['data']=true;
            }else{
                $data['error']=true;
                $data['message']='Error occurred while deleting data';
                $data['data']=false;
            }
        }else{
            $data['error']=true;
            $data['message']='No parameter received';
            $data['data']=false;
        }
        echo json_encode($data);
        */
    }

    public function delete_audio() {
        if($this->input->post()){
            $param['datetime'] = date('Y-m-d H:i:s');
            $param['table_name'] = $this->input->post('table_name');
            $param['id'] = $this->input->post('id');
            $param['column_name']=$this->input->post('column_name');

            $res=$this->do_common->delete_audio($param);
            if($res){
                $data['error']=false;
                $data['message']='Audio File Deleted Successfully';
                $data['data']=true;
            }else{
                $data['error']=true;
                $data['message']='Error occurred while deleting data';
                $data['data']=false;
            }
        } else {
            $data['error']=true;
            $data['message']='No parameter received';
            $data['data']=false;
        }
        echo json_encode($data);
    }

    public function toggle_status() {
        if($this->input->post()) {
            $param['datetime'] = date('Y-m-d H:i:s');
            $param['table_name'] = $this->input->post('table_name');
            $param['id'] = $this->input->post('id');
            $param['column_name'] = $this->input->post('column_name');
            $param['status'] = $this->input->post('status');
            $status_text = !empty($this->input->post('status')) ? 'Activated':'Deactivated';

            if($this->input->post('table_name') == 'category') {
                $caption_text = 'Category';

            } else  if($this->input->post('table_name') == 'sub_category') {
                $caption_text = 'Subcategory';

            } else  if($this->input->post('table_name') == 'sentences') {
                $caption_text = 'Sentence';

            } else {
                $caption_text = 'Data';
            }

            $res = $this->do_common->toggle_status($param);
            if($res){
                $data['error']=false;
                $data['message'] = $caption_text.' '.$status_text.' Successfully';

            }else{
                $data['error']=true;
                $data['message']='Error occurred while processing data';

            }
        } else {
            $data['error']=true;
            $data['message']='No parameter received';
            $data['data']=false;
        }
        echo json_encode($data);
    }
}