<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Createpdf extends CI_Controller {
    function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->load->helper('pdf_helper');

        $data = [
            'data' => 1
        ];
        $this->load->view('log/pdfreport', $data);
    }
}

