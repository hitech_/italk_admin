<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	public function __construct() {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET,OPTIONS,POST,DELETE");
        //header('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        //$this->load->language('english/form_validation_lang');
        $this->load->model(array('categories_model'));
    }

    public function get_subcategories(){
        $filter = $this->input->post();
        $where = [];
        if(!empty($filter['category'])) {
            $where['category_id'] = (int) $filter['category'];
        }
        $order_by = ['S.eng_name' => 'ASC'];
        $result = $this->categories_model->get_subcategories($where, 0, 0, $order_by, 'S.id, S.eng_name');

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        echo json_encode($response);
        exit;
    }
}