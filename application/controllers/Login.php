<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
	}

	public function index(){

		$this->load->view('includes/head');
		$this->load->view('login');
	}

	public function authenticate(){
		if($this->input->post()){
			$rescount=false;
			parse_str($this->input->post('formdata'), $param);
			if(($param['username']==USERNAME) && ($param['password']==PASSWORD)){
				$rescount=true;
			}

			if($rescount){
				$sessionparam['username']=USERNAME;
				$this->session->set_userdata($sessionparam);
				$data['error']=false;
				$data['message']='Successfully loged in';
				$data['data']='gone in';
			}else{
				$data['error']=true;
				$data['message']='Invalid Username and Password';
				$data['data']=false;
			}
		}else{
			$data['error']=true;
			$data['message']='Did not receive any parameter';
			$data['data']=false;
		}
		echo json_encode($data);
	}
	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
}