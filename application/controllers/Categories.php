<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {
    public $categorytablename, $subcategorytablename;
	public function __construct() {
        header("Content-type: text/html; charset=UTF-8");
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET,OPTIONS,POST,DELETE");
        /*header('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');*/
        parent::__construct();

        // load Pagination library
        $this->load->library('pagination');

        // load URL helper
        //$this->load->helper('url');

        $this->categorytablename='category';
        $this->subcategorytablename='sub_category';
        $this->load->model(array('categories_model'));
    }

    public function index(){
        $filter = $this->input->get();

        $data['pagetitle']='Categories';
        $data['activemenu']='categories';
        $data['filter']= $filter;

        $where = [];
        if(!empty($filter['category'])) {
            $where['id'] = $filter['category'];
        }
        //echo '<pre>'; print_r($filter); exit;

//        if(!empty($filter)) {
//            $this->form_validation->set_rules('category_name', 'Category', 'required');
//            if ($this->form_validation->run() === false) {
//                //error msg
//            }
//        }

        // load config file
        $this->config->load('pagination', TRUE);
        $settings = $this->config->item('pagination');

        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;
        $limit = $settings['per_page'];
        $offset = ($limit * $start_index) - $limit;
        $order_by = ['sequence_no' => 'ASC'];

        $data['categories']=$this->categories_model->get_categories([], 0, 0, ['eng_name' => 'ASC']);

        $total_records = $this->categories_model->get_total_categories($where);

        //$data['categories']=$this->categories_model->categories($params);
        $data['data'] = $this->categories_model->get_categories($where, $limit, $offset, $order_by);
        //echo '<pre>'; print_r($data);exit;


        $settings['total_rows'] = $total_records;;
        $settings['base_url'] = base_url() . 'index.php/categories/index';
        $this->pagination->initialize($settings);

        // build paging links
        $data["links"] = $this->pagination->create_links();

        $this->load->view('includes/header', $data);
        $this->load->view('categories', $data);
        $this->load->view('includes/footer');
    }

    public function exportExcel() {
        $filter = $this->input->get();

        $where = [];
        if(!empty($filter['category'])) {
            $where['id'] = $filter['category'];
        }

        $limit = 0;
        $offset = 0;
        $order_by = ['sequence_no' => 'ASC'];

        $result = $this->categories_model->get_categories($where, $limit, $offset, $order_by);
        $this->categories_model->exportCSVCategory($result);

    }

    public function add($catid=false){
        if($catid){
            $where['id'] = $catid;
            $order_by = ['eng_name' => 'ASC'];
            //$data['existingcatdata']=$this->categories_model->categories($param);
            $data['existingcatdata']=$this->categories_model->get_categories($where, 0, 0, $order_by);
        }
        //echo '<pre>'; print_r($data); exit;
        $data['pagetitle']='Add Categories';
        if($this->input->post()){
            $this->form_validation->set_rules('sequence_no', 'Sequence No', 'required');
            $this->form_validation->set_rules('eng_name', 'Category Name English', 'required');
            //$this->form_validation->set_rules('ar_name', 'Category Name Arabic', 'required');
            //$this->form_validation->set_rules('eng_ar_name', 'Category Name English-Arabic', 'required');
            //$this->form_validation->set_rules('ar_eng_name', 'Category Name Arabic-English', 'required');
            if($this->form_validation->run()===false){

            }else{
                $image = $_FILES['image'];
                $ar_audio = !empty($_FILES['ar_audio']) ? $_FILES['ar_audio'] : '';

                if(!$image){
                    $data['message']='Field Image is required';
                }else{
                    if(!empty($image['tmp_name'])){
                        //$imagedims=@getimagesize($_FILES['image']['tmp_name']);
                        //$imagewidth=$imagedims[0];
                        //$imageheight=$imagedims[1];

                        // if(($imagewidth < MINIMAGEWIDTHHEIGHT || $imageheight < MINIMAGEWIDTHHEIGHT) || ($imagewidth >MAXIMAGEWIDTHHEIGHT || $imageheight > MAXIMAGEWIDTHHEIGHT)){
                        //     $data['error']=true;
                        //     $data['message']='Image size should be between 600 x 600';
                        //     $data['data']=false;
                        //     $upload=false;
                        // }
                        // else{
                            $rawimagedata=file_get_contents($image['tmp_name']);
                            $insertparam['image']=base64_encode($rawimagedata);
                        // }
                    }

                    if(!empty($ar_audio['tmp_name'])){
                        $insertparam['ar_audio'] = base64_encode(file_get_contents($ar_audio['tmp_name']));

                    }

                    $insertparam['sequence_no']=$this->input->post('sequence_no');
                    $insertparam['eng_name']=$this->input->post('eng_name');
                    $insertparam['ar_name']=$this->input->post('ar_name');
                    $insertparam['eng_ar_name']=$this->input->post('eng_ar_name');
                    $insertparam['ar_eng_name']=$this->input->post('ar_eng_name');

                    //echo '<pre>'; print_r($insertparam); exit;
                    if($catid){
                        $insertparam['updated_at']=date('Y-m-d H:i:s');
                    }else{
                        $insertparam['created_at']=date('Y-m-d H:i:s');
                        $insertparam['updated_at']=date('Y-m-d H:i:s');
                    }
                    $res=$this->categories_model->add($insertparam, $param['catid']);
                    if($res){
                        $data['error']=false;
                        $data['message']='Category added Successfully';
                        $data['data']=true;
                    }else{
                        $data['error']=true;
                        $data['message']='Error occurred while adding category';
                        $data['data']=false;
                    }
                }
                $this->session->set_flashdata('flashmessage', $data['message']);
            }
        }
        $this->load->view('includes/header', $data);
        $this->load->view('addcategories', $data);
        $this->load->view('includes/footer');
    }

    public function subcategories(){

        $filter = $this->input->get();
        //echo '<pre>'; print_r($filter);exit;
        $data['pagetitle']='Subcategories';
        $data['activemenu']='subcategories';
        $data['filter']= $filter;

        if(!empty($filter)) {
//            $this->form_validation->set_rules('category_name', 'Category', 'required');
//            if ($this->form_validation->run() === false) {
//                //error msg
//            }
        }

        // load config file
        $this->config->load('pagination', TRUE);
        $settings = $this->config->item('pagination');

        // init params
        $where = array();
        if(!empty($filter['category'])) {
            $where['category_id'] = $filter['category'];

        }

        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;
        $limit = $settings['per_page'];
        $offset = ($limit * $start_index) - $limit;

        $total_records = $this->categories_model->get_total_subcategories($where);
        //echo $total_records,'-',$limit,'-',$offset;

        //$data['categories']=$this->categories_model->categories($params);
        $data['data'] = $this->categories_model->get_subcategories($where, $limit, $offset, ['S.sequence_no' => 'ASC']);
        //echo '<pre>'; print_r($data);exit;

        $settings['total_rows'] = $total_records;
        $settings['base_url'] = base_url() . 'index.php/categories/subcategories';
        //echo '<pre>'; print_r($settings); exit;
        $this->pagination->initialize($settings);

        // build paging links
        $data["links"] = $this->pagination->create_links();


        //$data['subcategories']=$this->categories_model->subcategories();
        $data['categories']=$this->categories_model->get_categories([], 0, 0, ['eng_name' => 'ASC']);
        //echo '<pre>'; print_r($where); exit;

        $this->load->view('includes/header', $data);
        $this->load->view('subcategories', $data);
        $this->load->view('includes/footer');
    }

    public function exportExcelSubcategory() {
        $filter = $this->input->get();
        $this->categories_model->exportCSVSubCategory($filter);

    }

    public function addsubcategory($subcatid=false, $parentcat=false){
        //echo '<pre>'; print_r($this->input->post()); exit;

        $sc_ar_audio = !empty($_FILES['sc_ar_audio']) ? $_FILES['sc_ar_audio'] : '';
        $arabic_audio = !empty($_FILES['arabic_audio']) ? $_FILES['arabic_audio'] : '';

        $data['parentcats']=$this->categories_model->get_categories([], 0, 0, ['eng_name' => 'ASC']);
        if($subcatid && !$parentcat){
            $existingsubcatdata=array();
            $param['subcatid']=$subcatid;
            $rawexistingsubcatdata=$this->categories_model->subcategories($param);
            //echo '<pre>';print_r($rawexistingsubcatdata); die;
            if(!empty($rawexistingsubcatdata)){
                foreach($rawexistingsubcatdata as $key=>$val){
                    $existingsubcatdata['sequence_no']=$val->sequence_no;
                    $existingsubcatdata['id']=$val->id;
                    $existingsubcatdata['engsubcatname']=$val->engsubcatname;
                    $existingsubcatdata['arsubcatname']=$val->arsubcatname;
                    $existingsubcatdata['parentcatid']=$val->parentcatid;
                    $existingsubcatdata['sc_image']=$val->sc_image;
                    $existingsubcatdata['engparentcatname']=$val->engparentcatname;
                    $existingsubcatdata['arparentcatname']=$val->arparentcatname;
                    $existingsubcatdata['created_at']=$val->created_at;
                    $existingsubcatdata['eng_ar_name']=$val->eng_ar_name;
                    $existingsubcatdata['ar_eng_name']=$val->ar_eng_name;
                    $existingsubcatdata['sc_ar_audio']=$val->sc_ar_audio;
                    if(!empty($val->sentence_id)){
                        //echo '<pre>'; print_r($val); exit;
                        $existingsubcatdata['sentence'][] = [
                            'sentence_id'=>$val->sentence_id,
                            'sentence_en'=>$val->sentence_en,
                            'sentence_ar'=>$val->sentence_ar,
                            'sentence_en_ar'=>$val->sentence_en_ar,
                            'sentence_ar_en'=>$val->sentence_ar_en,
                            'sentence_ar_audio'=>$val->sentence_ar_audio,
                            'status'=>$val->status
                        ];
                    }
                }
                $data['existingsubcatdata']=$existingsubcatdata;
//                echo '<pre>';
//                print_r($data['existingsubcatdata']['sentence']); die;
            }
        }
        if($parentcat){
            $data['parentcat']=$parentcat;
        }
        $data['pagetitle']='Add Subcategories';
        if($this->input->post()){
            //echo '<pre>'; print_r($arabic_audio); exit;
            $this->form_validation->set_rules('sequence_no', 'Sequence No', 'required');
            $this->form_validation->set_rules('sub_eng_name', 'Subcategory Name English', 'required');
            //$this->form_validation->set_rules('sub_ar_name', 'Subcategory Name Arabic', 'required');
            //$this->form_validation->set_rules('sub_eng_ar_name', 'Subcategory Name English-Arabic', 'required');
            //$this->form_validation->set_rules('sub_ar_eng_name', 'Subcategory Name Arabic-English', 'required');
            $this->form_validation->set_rules('parent_cat', 'Parent Category', 'required');
            $this->form_validation->set_rules('englishsentence[]', 'English Sentence', 'required', array('required'=>'You must at least submit one english sentence'));
            //$this->form_validation->set_rules('arabicsentence[]', 'Arabic Sentence', 'required', array('required'=>'You must at least submit one arabic sentence'));
            //$this->form_validation->set_rules('arabicenglishsentence[]', 'Arabic-English Sentence', 'required', array('required'=>'You must at least submit one arabic-english sentence'));
            //$this->form_validation->set_rules('englisharabicsentence[]', 'English-Arabic Sentence', 'required', array('required'=>'You must at least submit one english-arabic sentence'));
            if($this->form_validation->run()===false){

            }else{
                //print_r($this->input->post()); die;
                if(!$_FILES['sub_image']){
                    $data['message']='Field Image is required';
                }else{
                    if(!empty($_FILES['sub_image']['tmp_name'])){
                        //$imagedims=@getimagesize($_FILES['sub_image']['tmp_name']);
                        //$imagewidth=$imagedims[0];
                        //$imageheight=$imagedims[1];
                        // if(($imagewidth < MINIMAGEWIDTHHEIGHT || $imageheight < MINIMAGEWIDTHHEIGHT) || ($imagewidth > MAXIMAGEWIDTHHEIGHT || $imageheight > MAXIMAGEWIDTHHEIGHT)){
                        //     $data['error']=true;
                        //     $data['message']='Image size should be between 600 x 600';
                        //     $data['data']=false;
                        //     $upload=false;
                        // }
                        // else{
                            $rawimagedata=file_get_contents($_FILES['sub_image']['tmp_name']);
                            $insertparam['sc_image']=base64_encode($rawimagedata);
                        // } 
                    }

                    if(!empty($sc_ar_audio['tmp_name'])){
                        $insertparam['sc_ar_audio'] = base64_encode(file_get_contents($sc_ar_audio['tmp_name']));
                    }
                    $insertparam['sequence_no']=$this->input->post('sequence_no');
                    $insertparam['eng_name']=$this->input->post('sub_eng_name');
                    $insertparam['category_id']=$this->input->post('parent_cat');
                    $insertparam['ar_name']=$this->input->post('sub_ar_name');
                    $insertparam['eng_ar_name']=$this->input->post('sub_eng_ar_name');
                    $insertparam['ar_eng_name']=$this->input->post('sub_ar_eng_name');
                    $insertparam['updated_at']=date('Y-m-d H:i:s');
                    $insertparam['created_at']=date('Y-m-d H:i:s');

                    $sentenceparam['arabicsentence']=$this->input->post('arabicsentence');
                    $sentenceparam['englishsentence']=$this->input->post('englishsentence');
                    $sentenceparam['englisharabicsentence']=$this->input->post('englisharabicsentence');
                    $sentenceparam['arabicenglishsentence']=$this->input->post('arabicenglishsentence');
                    $sentenceparam['arabic_audio'] = $arabic_audio;
                    $sentenceparam['sentence_id'] = $this->input->post('hdSentenceId');

                    //echo '<pre>'; print_r($sentenceparam); exit;
                    $res=$this->categories_model->addsubcategory($insertparam, $sentenceparam, $param['subcatid']);
                    //echo 'done'; exit;
                    if($res){
                        $data['error']=false;
                        $data['message']='Subcategory added Successfully';
                        $data['data']=true;
                    }else {
                        $data['error'] = true;
                        $data['message'] = 'Error occurred while adding subcategory';
                        $data['data'] = false;
                    }
                }
                $this->session->set_flashdata('flashmessage', $data['message']);
            }
        }
        $this->load->view('includes/header', $data);
        $this->load->view('addsubcategories', $data);
        $this->load->view('includes/footer');
    }

    public function bulkeditsubcategory(){
        $filter = $this->input->get();

        if($this->input->post()){
            $sc_ar_audio = !empty($_FILES['sc_ar_audio']) ? $_FILES['sc_ar_audio'] : '';
            $arabic_audio = !empty($_FILES['arabic_audio']) ? $_FILES['arabic_audio'] : '';
            $category = $this->input->post('category');
            $subcategory = $this->input->post('subcategory');
            $filter = [
                'category' => $category,
                'subcategory' => $subcategory
            ];

            $this->form_validation->set_rules('sequence_no', 'Sequence No', 'required');
            $this->form_validation->set_rules('sub_eng_name', 'Subcategory Name English', 'required');
            //$this->form_validation->set_rules('sub_ar_name', 'Subcategory Name Arabic', 'required');
            //$this->form_validation->set_rules('sub_eng_ar_name', 'Subcategory Name English-Arabic', 'required');
            //$this->form_validation->set_rules('sub_ar_eng_name', 'Subcategory Name Arabic-English', 'required');
            $this->form_validation->set_rules('parent_cat', 'Parent Category', 'required');
            $this->form_validation->set_rules('englishsentence[]', 'English Sentence', 'required', array('required'=>'You must at least submit one english sentence'));
            //$this->form_validation->set_rules('arabicsentence[]', 'Arabic Sentence', 'required', array('required'=>'You must at least submit one arabic sentence'));
            //$this->form_validation->set_rules('arabicenglishsentence[]', 'Arabic-English Sentence', 'required', array('required'=>'You must at least submit one arabic-english sentence'));
            //$this->form_validation->set_rules('englisharabicsentence[]', 'English-Arabic Sentence', 'required', array('required'=>'You must at least submit one english-arabic sentence'));
            if($this->form_validation->run()===false){

            }else{
                //print_r($this->input->post()); die;
                if(!$_FILES['sub_image']){
                    $data['message']='Field Image is required';
                }else{
                    if(!empty($_FILES['sub_image']['tmp_name'])){
                        //$imagedims=@getimagesize($_FILES['sub_image']['tmp_name']);
                        //$imagewidth=$imagedims[0];
                        //$imageheight=$imagedims[1];
                        // if(($imagewidth < MINIMAGEWIDTHHEIGHT || $imageheight < MINIMAGEWIDTHHEIGHT) || ($imagewidth > MAXIMAGEWIDTHHEIGHT || $imageheight > MAXIMAGEWIDTHHEIGHT)){
                        //     $data['error']=true;
                        //     $data['message']='Image size should be between 600 x 600';
                        //     $data['data']=false;
                        //     $upload=false;
                        // }
                        // else{
                        $rawimagedata=file_get_contents($_FILES['sub_image']['tmp_name']);
                        $insertparam['sc_image']=base64_encode($rawimagedata);
                        // }
                    }

                    if(!empty($subcategory)) {
                        if(!empty($sc_ar_audio['tmp_name'])){
                            $insertparam['sc_ar_audio'] = base64_encode(file_get_contents($sc_ar_audio['tmp_name']));
                        }
                        $insertparam['sequence_no']=$this->input->post('sequence_no');
                        $insertparam['eng_name']=$this->input->post('sub_eng_name');
                        $insertparam['category_id']=$this->input->post('parent_cat');
                        $insertparam['ar_name']=$this->input->post('sub_ar_name');
                        $insertparam['eng_ar_name']=$this->input->post('sub_eng_ar_name');
                        $insertparam['ar_eng_name']=$this->input->post('sub_ar_eng_name');
                        $insertparam['updated_at']=date('Y-m-d H:i:s');
                        $insertparam['created_at']=date('Y-m-d H:i:s');

                        $sentenceparam['arabicsentence']=$this->input->post('arabicsentence');
                        $sentenceparam['englishsentence']=$this->input->post('englishsentence');
                        $sentenceparam['englisharabicsentence']=$this->input->post('englisharabicsentence');
                        $sentenceparam['arabicenglishsentence']=$this->input->post('arabicenglishsentence');
                        $sentenceparam['arabic_audio'] = $arabic_audio;
                        $sentenceparam['sentence_id'] = $this->input->post('hdSentenceId');

                        //echo '<pre>'; print_r($sentenceparam); exit;
                        $res = $this->categories_model->addsubcategory($insertparam, $sentenceparam, $subcategory);
                        if($res){
                            $data['error']=false;
                            $data['message']='Subcategory updated Successfully';
                            $data['data']=true;
                        }else{
                            $data['error']=true;
                            $data['message']='Error occurred while updating subcategory';
                            $data['data']=false;
                        }
                    } else {
                        $data['error']=true;
                        $data['message']='Error occurred while updating subcategory';
                        $data['data']=false;
                    }
                }

                $this->session->set_flashdata('flashmessage', $data['message']);
            }
        }

        $data['filter'] = $filter;

        $data['pagetitle']='Bulk Edit Subcategory';
        $data['categories']=$this->categories_model->get_categories([], 0, 0, ['eng_name' => 'ASC']);
        //echo '<pre>'; print_r($data);exit;

        if(!empty($filter['category'])) {
            $where = [];
            $where['category_id'] = (int) $filter['category'];

            $order_by = ['S.sequence_no' => 'ASC'];
            $data['subcategories'] = $this->categories_model->get_subcategories($where, 0, 0, $order_by, 'S.id, S.eng_name');
        }

        //echo '<pre>'; print_r($data); exit;

        if(!empty($filter['subcategory'])) {
            $existingsubcatdata = [];
            $where = [];
            $where['subcatid'] = (int) $filter['subcategory'];
            $rawexistingsubcatdata = $this->categories_model->subcategories($where);
            //echo '<pre>';
            //print_r($rawexistingsubcatdata); die;
            if(!empty($rawexistingsubcatdata)){
                foreach($rawexistingsubcatdata as $key=>$val){
                    $existingsubcatdata['sequence_no']=$val->sequence_no;
                    $existingsubcatdata['id']=$val->id;
                    $existingsubcatdata['engsubcatname']=$val->engsubcatname;
                    $existingsubcatdata['arsubcatname']=$val->arsubcatname;
                    $existingsubcatdata['parentcatid']=$val->parentcatid;
                    $existingsubcatdata['sc_image']=$val->sc_image;
                    $existingsubcatdata['engparentcatname']=$val->engparentcatname;
                    $existingsubcatdata['arparentcatname']=$val->arparentcatname;
                    $existingsubcatdata['created_at']=$val->created_at;
                    $existingsubcatdata['eng_ar_name']=$val->eng_ar_name;
                    $existingsubcatdata['ar_eng_name']=$val->ar_eng_name;
                    $existingsubcatdata['sc_ar_audio']=$val->sc_ar_audio;
                    if(!empty($val->sentence_id)){
                        $existingsubcatdata['sentence'][] = [
                            'sentence_id'=>$val->sentence_id,
                            'sentence_en'=>$val->sentence_en,
                            'sentence_ar'=>$val->sentence_ar,
                            'sentence_en_ar'=>$val->sentence_en_ar,
                            'sentence_ar_en'=>$val->sentence_ar_en,
                            'sentence_ar_audio'=>$val->sentence_ar_audio
                        ];
                    }
                }
                $data['existingsubcatdata']=$existingsubcatdata;
                //echo '<pre>';
                //print_r($data['existingsubcatdata']); die;
            }
        }

        $this->load->view('includes/header', $data);
        $this->load->view('bulkeditsubcategories', $data);
        $this->load->view('includes/footer');
    }

    public function updatetable() {
        $this->categories_model->updatetable();
    }
}