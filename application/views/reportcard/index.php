<div class="content logs-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header logs-head error_class">
                        <h3 class="text-center">Report Card</h3>
                        <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <?php echo form_open('reportcard', array('id' => 'reportcardForm', 'method'=> 'get')); ?>
                                <div class="form-group">
                                <label for="disabilitytype">Users</label>
                                    <select class="form-control" name="users" id="users">
                                    <option value="">Select User</option><?php
                                        foreach($users as $val){
                                            if($val->u_user_id == $filter['users']){
                                                $selected='selected';
                                            }else{
                                                $selected='';
                                            }
                                            ?><option value="<?php echo $val->u_user_id?>" <?php echo $selected; ?>><?php echo $val->u_username; ?> [<?php echo $val->u_email_id; ?>]</option><?php
                                        }
                                    ?></select>
                                </div>
                                <button type="submit" class="btn btn-info btn-fill submit_btn">Get Report</button>
                                &nbsp;
                                <button type="button" class="btn btn-info btn-fill download_report">Download Report</button>
                            <?php echo form_close(); ?>
                        </div>
                        </div>
                    </div><?php
                    if(!empty($filter['users'])) {
                        ?><div class="filter-by">
                            <p>Report card of <span id="filterByText"><?php echo ucwords(strtolower($users[$filter['users']]->u_username));?></p>
                        </div><?php
                    }
                    ?><div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Sr#</th>
                                    <th>Category</th>
                                    <th>Result</th>
                                </tr>
                            </thead>
                            <tbody><?php
                            if(!empty($category_wise_cnt)){
                                $i=0;
                                //echo '<pre>'; print_r($logs); exit;
                                foreach($category_wise_cnt as $key=>$val){
                                    $i++;

                                    $category = !empty($val->category) ? ucwords(strtolower($val->category)) : '-';
                                    $category_id = !empty($val->id) ? $val->id : 0;
                                    $category_cnt = !empty($val->cnt) ? $val->cnt : '-';
                                    $success_cnt = !empty($log_report[$category_id]->success_cnt) ? $log_report[$category_id]->success_cnt : 0;

                                    ?><tr>
                                        <td><?php echo $i;?></td>
                                        <td><?php echo $category; ?></td>
                                        <td><?php echo $success_cnt .' / '. $category_cnt; ?></td>
                                    </tr><?php
                                }
                            }
                            ?></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(".submit_btn").on("click", function(e){
    e.preventDefault();
    $('#reportcardForm').attr('action', "<?php echo base_url('index.php/reportcard');?>").submit();
});

$(".download_report").on("click", function(e){
    $('#reportcardForm').attr('action', "<?php echo base_url('index.php/reportcard/exportExcelReportCard');?>").submit();
});
</script>
