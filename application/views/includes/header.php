<?php include('head.php'); 
    
    if(!$this->session->userdata('username')){
        header('location: '.base_url().'');
    }
?>
<body>

    <!-- Logs Filter Modal -->
<div class="modal fade modal-mini modal-primary" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form action="" class="logs-filter-date">
                    <div class="form-group">
                        <label for="">From Date</label>
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" data-toggle="datepicker" name="selectfromdate">
                            <div class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">To Date</label>
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" data-toggle="datepicker" name="selecttodate">
                            <div class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </div>
                        </div>
                  </div>
                    <button type="button" class="btn btn-info btn-fill" id="storedates">Set</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--  End Logs Filter Modal -->

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="<?php echo base_url(); ?>assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="<?php echo base_url();?>index.php/categories" class="simple-text">
                    iTalk
                </a>
            </div>

            <ul class="nav">
                <li class="<?php echo ($activemenu=='categories') ? 'active' : ''; ?>">
                    <a href="<?php echo base_url();?>index.php/categories">
                        <i class="pe-7s-graph"></i>
                        <p>Categories</p>
                    </a>
                </li>
                <li class="<?php echo ($activemenu=='subcategories') ? 'active' : ''; ?>">
                    <a href="<?php echo base_url();?>index.php/categories/subcategories">
                        <i class="pe-7s-user"></i>
                        <p>Sub Categories</p>
                    </a>
                </li>
                <li class="<?php echo ($this->uri->segment(1)=='logs') ? 'active' : ''; ?>">
                    <a href="<?php echo base_url();?>index.php/logs">
                        <i class="pe-7s-user"></i>
                        <p>Logs</p>
                    </a>
                </li>
                <li class="<?php echo ($this->uri->segment(1)=='reportcard') ? 'active' : ''; ?>">
                    <a href="<?php echo base_url();?>index.php/reportcard">
                        <i class="pe-7s-user"></i>
                        <p>User Report Card</p>
                    </a>
                </li>
                <!--<li>
                    <a href="table.html">
                        <i class="pe-7s-note2"></i>
                        <p>Table List</p>
                    </a>
                </li>
                <li>
                    <a href="typography.html">
                        <i class="pe-7s-news-paper"></i>
                        <p>Typography</p>
                    </a>
                </li>
                <li>
                    <a href="icons.html">
                        <i class="pe-7s-science"></i>
                        <p>Icons</p>
                    </a>
                </li>
                <li>
                    <a href="maps.html">
                        <i class="pe-7s-map-marker"></i>
                        <p>Maps</p>
                    </a>
                </li>
                <li>
                    <a href="notifications.html">
                        <i class="pe-7s-bell"></i>
                        <p>Notifications</p>
                    </a>
                </li>
				<li class="active-pro">
                    <a href="upgrade.html">
                        <i class="pe-7s-rocket"></i>
                        <p>Home</p>
                    </a>
                </li>-->
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><?php echo $pagetitle; ?></a>
                </div>
                <div class="collapse navbar-collapse">
                    <!--<ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
								<p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret hidden-lg hidden-md"></b>
									<p class="hidden-lg hidden-md">
										5 Notifications
										<b class="caret"></b>
									</p>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
                        <li>
                           <a href="">
                                <i class="fa fa-search"></i>
								<p class="hidden-lg hidden-md">Search</p>
                            </a>
                        </li>
                    </ul>-->

                    <!--<ul class="nav navbar-nav navbar-right">
                        <li>
                           <a href="">
                               <p>Account</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <p>
										Dropdown
										<b class="caret"></b>
									</p>

                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                              </ul>
                        </li>
                        <li>
                            <a href="#">
                                <p>Log out</p>
                            </a>
                        </li>
						<li class="separator hidden-lg"></li>
                    </ul>-->
                    
                    <a class="btn btn-info btn-fill white-text pull-right" href="<?php echo base_url();?>index.php/login/logout">Log out</a>
                   
                </div>
            </div>
        </nav>