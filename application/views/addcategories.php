<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header error_class">
                        <h4 class="title"><?php echo $pagetitle; ?></h4>
                        <p><?php //echo validation_errors()?></p>
                    </div>
                    <div class="content flashmessage" data-flashdata="<?php echo $this->session->flashdata('flashmessage'); ?>" data-error=<?php echo $error; ?>>
                        <form class="categoryform" method="POST" action="?" enctype="multipart/form-data" accept-charset="utf-8">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Sequence No</label>
                                        <input type="text" class="form-control" placeholder="Sequence No" name="sequence_no" value="<?php echo $existingcatdata[0]->sequence_no; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Category Name (English)</label>
                                        <input type="text" class="form-control" placeholder="Cagegory Name (English)" name="eng_name" value="<?php echo $existingcatdata[0]->eng_name; ?>">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Category Name (Arabic)</label>
                                        <input type="text" class="form-control" placeholder="Cagegory Name (Arabic)" name="ar_name" value="<?php echo $existingcatdata[0]->ar_name; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Category Name (English-Arabic)</label>
                                        <input type="text" class="form-control" placeholder="Category Name (English-Arabic)" name="eng_ar_name" value="<?php echo $existingcatdata[0]->eng_ar_name;?>">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Category Name (Arabic-English)</label>
                                        <input type="text" class="form-control" placeholder="Category Name (Arabic-English)" name="ar_eng_name" value="<?php echo $existingcatdata[0]->ar_eng_name; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 <?php echo ($existingcatdata) ? 'displaynoneclass' : ''; ?>">
                                    <div class="form-group">
                                        <label for="categorystatus">Image</label>
                                        <input type="file" class="form-control" placeholder="Please select an image" name="image" value="">
                                    </div>
                                </div>
                                <?php if($existingcatdata){ ?>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="categorystatus">Existing Image</label><br>
                                        <img src="data:image/jpeg;base64, <?php echo $existingcatdata[0]->image; ?>" width="100" height="100" class="editedimage">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </div>
                                    <p class="filename"></p>
                                </div>
                                <?php } ?>

                                <div class="col-md-5 <?php echo ($existingcatdata[0]->ar_audio) ? 'displaynoneclass' : ''; ?>">
                                    <div class="form-group">
                                        <label for="categorystatus">Arabic Audio</label>
                                        <input type="file" class="form-control" placeholder="Please select an arabic audio" name="ar_audio" value="">
                                    </div>
                                </div>

                                <?php if($existingcatdata[0]->ar_audio){ ?>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="categorystatus">Existing Arabic Audio</label><br>
                                        <audio controls src="data:audio/ogg;base64,<?php echo $existingcatdata[0]->ar_audio;?>" />
                                    </div>
                                    <span><a href="javascript:void(0);" onclick="delete_audio('category','ar_audio','<?php echo $existingcatdata[0]->id;?>')">Delete</a></span>
                                </div>
                                <?php } ?>
                            </div>
                            <button type="submit" class="btn btn-info btn-fill pull-right">Save Category</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="flashmodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p><?php echo $this->session->flashdata('flashmessage'); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closemodalfooter" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
var error=jQuery('.flashmessage').data('error');
jQuery(document).ready(function(){
    var flashdata=jQuery('.flashmessage').data('flashdata');
    console.log(flashdata);
    if(flashdata){
        jQuery('#flashmodal').modal('show');
    }
});

jQuery('.close, .closemodalfooter').on('click', function(){
    if(!error){
        var current_url =window.location.href;
       // window.location.href='<?php //echo base_url(); ?>index.php/categories';
       window.location.href=current_url;
    }
})

jQuery('.editedimage').on('mouseover', function(){
    jQuery(this).css({'opacity':'0.2'});
    jQuery(this).next('i').show();
    jQuery(this).addClass('editnow');
})

jQuery('.editedimage').on('mouseout', function(){
    jQuery(this).css({'opacity':''});
    jQuery(this).next('i').hide();
    jQuery(this).removeClass('editnow');
})

jQuery('.editedimage, .fa-pencil-square-o').on('click', function(){
    if(jQuery(this).hasClass('editnow')){
        jQuery('input[name=image]').click();
    }
})

jQuery('input[name=image]').on('change', function(){
    jQuery('.filename').html('');
    filename=jQuery(this).val();
    jQuery('.filename').append(filename);
})

function delete_audio(table_name, column_name, id) {
    var table_name = $.trim(table_name);
    var column_name = $.trim(column_name);
    var id = $.trim(id);

    if(table_name != '' && column_name != '' && id != '') {
        if(confirm('Are you sure, you want to delete this audio file?')) {
            jQuery.ajax({
                type:'POST',
                data:{table_name, column_name, id},
                url:'<?php echo base_url(); ?>index.php/common/delete_audio'
            }).done(function(data){
                rawdata=jQuery.parseJSON(data);
                if(rawdata.error==false){
                    $('#flashmodal .modal-body p').text(rawdata.message);
                    jQuery('#flashmodal').modal('show');
                }
            });
        }
    }
}
</script>