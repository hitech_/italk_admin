<div class="container">
	<div class="login-wrapper">
		<form action="" method="post" name="Login_Form" class="form-signin">       
		    <h3 class="form-signin-heading">Welcome! Please Sign In</h3>
			  <hr class="colorgraph"><br>
			  
			  <input type="text" class="form-control" name="username" placeholder="Username" required="" autofocus="" />
			  <input type="password" class="form-control" name="password" placeholder="Password" required=""/>     		  
			 
			  <button class="btn btn-lg btn-primary btn-block" name="Submit" value="Login" type="submit">Login</button>  			
		</form>			
	</div>
</div>
<script>
jQuery('button').on('click', function(e){
	e.preventDefault();
	formdata=jQuery('.form-signin').serialize();
	jQuery.ajax({
		type:'POST',
		data:{formdata},
		url:'<?php echo base_url(); ?>index.php/login/authenticate'
	}).done(function(data){
		rawdata=jQuery.parseJSON(data);
		if(rawdata.error!=true){
			window.location.href='<?php echo base_url();?>index.php/categories';
		}else{
			alert(rawdata.message);
		}
	})
})
</script>