<style type="text/css">
.dataTables_length{
display: none;
}
.dataTables_filter{
display: none;}
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header pagetitle">
                        <h4 class="title"><?php echo $pagetitle; ?></h4>
                        <!--<a href="<?php echo base_url(); ?>index.php/categories/addsubcategory">Add a Subcategory</a>-->
                    </div>
                    <div class="col-md-12 search-wrap">
                        <?php echo form_open('categories/subcategories', array('id' => 'subcategoryForm', 'method'=> 'get')); ?>
                        <div class="">
                            <label>Search By Sub Category</label>
                        </div>
                        <div class="">
                            <select name="category" class="form-control">
                                <option value="">Select Category</option>
                               <?php
                                foreach ($categories as $c_key => $c_value) {
                                ?>
                                    <option value="<?php echo $c_value->id ?>" <?php echo ($filter['category'] == $c_value->id) ? 'selected' : '';?>><?php echo $c_value->eng_name?></option>
                                <?php
                                }
                               ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info btn-fill submit_btn">Search</button>
                        &nbsp;
                        <button type="button" class="btn btn-info btn-fill download_report">Download Report</button>
                        &nbsp;
                        <button type="button" class="btn btn-info btn-fill bulk_editsubcategory">Bulk Edit</button>
                        <?php echo form_close(); ?>
                    </div>
                    <!-- <div class="col-md-4">
                            <select name="category_ar" class="form-control col-md-3 drpdown">
                                <option value=""> Search By Arabic Category</option>
                               <?php
                                foreach ($categories as $ar_key => $ar_value) {
                                ?>
                                    <option value="<?php echo $ar_value->id ?>" <?php echo ($filter['category_ar'] == $ar_value->id) ? 'selected' : '';?>><?php echo $ar_value->ar_name?></option>
                                <?php
                                }
                               ?>
                            </select>
                        </div> -->
                    
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                	<th>Subcategory Name</th>
                                	<th>Sequence</th>
                                    <th>Parent Category Name</th>
                                    <th>Added Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($data as $key=>$val){
                                ?>
                                    <tr class="row_<?php echo $val->id; ?>">
                                        <td><img src="data:image/jpeg;base64, <?php echo $val->sc_image; ?>" width="100" height="100"></td>
                                        <td>
                                            <div><b>English:</b> <?php echo $val->engsubcatname; ?></div>
                                            <div><b>Arabic:</b> <?php echo $val->arsubcatname; ?></div>
                                            <div><b>English-Arabic:</b> <?php echo $val->eng_ar_name; ?></div>
                                            <div><b>Arabic-English:</b> <?php echo $val->ar_eng_name; ?></div>
                                        </td>
                                        <td><?php echo $val->sequence_no; ?></td>
                                        <td>
                                            <p><?php echo $val->engparentcatname; ?></p>
                                            <p><?php echo $val->arparentcatname; ?></p>
                                        </td>
                                        <td><?php echo date('dS M Y ', strtotime($val->created_at)); ?></td>
                                        <td>
                                            <label class="switch">
                                                <input type="checkbox" class="chk_status" value="<?php echo $val->id;?>" <?php echo ($val->status) ? 'checked' : '';?>>
                                                <span class="slider round"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url().'index.php/categories/addsubcategory/'.$val->id; ?>">EDIT</a>
                                            &nbsp;
                                            <a href="javascript: void(0)" onClick="delete_subcategory('sub_category', <?php echo $val->id; ?>)">DELETE</a>
                                        </td>
                                    </tr>            
                                <?php    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
         </div>

        <?php
        if (isset($links)) {
            ?><div class="row"><div class="col-md-12"><?php
                echo $links;
                ?></div></div><?php
        }
        ?>
    </div>
</div>
<script>

function delete_subcategory(table_name, id) {
    var table_name = $.trim(table_name);
    var id = $.trim(id);

    if(table_name != '' && id != '') {
        if(confirm('Are you sure, you want to delete this subcategory?')) {
            jQuery.ajax({
                type:'POST',
                data:{table_name, id},
                url:'<?php echo base_url(); ?>index.php/common/delete'
            }).done(function(data){
                rawdata=jQuery.parseJSON(data);
                if(rawdata.error==false){
                    jQuery('.row_'+id).fadeOut('slow');
                }
            });
        }
    }
}

/*
function deleteadminsubcategories(module, id, prefix=false){
    if(confirm('Are you sure, you want to delete this subcategory?')) {
        jQuery.ajax({
            type: 'POST',
            data: {module, id, prefix},
            url: '<?php echo base_url(); ?>index.php/common/delete'
        }).done(function (data) {
            rawdata = jQuery.parseJSON(data);
            if (rawdata.error == false) {
                jQuery('.row_' + id).fadeOut('slow');
            }
        });
    }
}
*/

function toggleStatus(table_name, id, column_name, status, $this) {
    var status_text = (status == 1) ? 'activate':'deactivate';
    if(confirm('Are you sure, you want to '+status_text+' this subcategory?')) {
        jQuery.ajax({
            type:'POST',
            data:{table_name, id, column_name, status},
            url:'<?php echo base_url(); ?>index.php/common/toggle_status',
            dataType: 'JSON'

        }).done(function(data){
            if(!data.error) {
                alert(data.message);
            } else {
                alert('Something went wrong, please try after sometime');
                window.location.reload();
            }

        }).fail(function (jqXHR, textStatus){
            alert('Something went wrong, please try after sometime');
            window.location.reload();
        });
    } else {
        $this.prop('checked', !$this.is(':checked'));
    }
}

$(document).ready(function() {
    $('.chk_status').change(function(){
        var table_name = 'sub_category';
        var id = $(this).val();
        var column_name = 'status';
        var status = ($(this).is(':checked')) ? 1 : 0;
        var $this = $(this);
        toggleStatus(table_name, id, column_name, status, $this);
    });

    $(".submit_btn").on("click", function(e){
        e.preventDefault();
        $('#subcategoryForm').attr('action', "<?php echo base_url('index.php/categories/subcategories');?>").submit();
    });

    $(".download_report").on("click", function(e){
        $('#subcategoryForm').attr('action', "<?php echo base_url('index.php/categories/exportExcelSubcategory');?>").submit();
    });

    $(".bulk_editsubcategory").on("click", function(e){
        window.location.replace("<?php echo base_url('index.php/categories/bulkeditsubcategory');?>");
    });
});
</script>