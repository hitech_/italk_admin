<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header pagetitle">
                        <h4 class="title"><?php echo $pagetitle; ?></h4>
                        <a class="btn btn-info btn-fill" href="<?php echo base_url(); ?>index.php/categories/add">Add a Category</a>
                    </div>

                    <div class="col-md-12 search-wrap">
                        <form id="categoryForm" method="GET" action="">
                        <div>
                            <label>Search By Category</label>
                        </div>
                        <div class="">
                            <!-- <select name="category" class="selectpicker" data-live-search="true"> -->
                            <select name="category" class="form-control">
                                <option value="">Select Category</option>
                               <?php
                                foreach ($categories as $c_key => $c_value) {
                                    ?>
                                    <option value="<?php echo $c_value->id ?>" <?php echo ($filter['category'] == $c_value->id) ? 'selected' : '';?>><?php echo $c_value->eng_name?></option>
                                    <?php
                                }
                              ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info btn-fill submit_btn">Search</button>
                            &nbsp;
                        <button type="button" class="btn btn-info btn-fill download_report">Download Report</button>
                        </form>
                    </div>

                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                	<th>Category Name</th>
                                	<th>Sequence</th>
                                    <th>Added Date</th>
                                    <th>Status</th>
                                    <th>Add sub-category</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody><?php
                            foreach($data as $key=>$val){
                                ?><tr class="row_<?php echo $val->id; ?>">
                                    <td><img src="data:image/jpeg;base64, <?php echo $val->image; ?>" width="100" height="100"></td>
                                    <td>
                                        <div><b>English:</b> <?php echo $val->eng_name; ?></div>
                                        <div><b>Arabic:</b> <?php echo $val->ar_name; ?></div>
                                        <div><b>English-Arabic:</b> <?php echo $val->eng_ar_name; ?></div>
                                        <div><b>Arabic-English:</b> <?php echo $val->ar_eng_name; ?></div>
                                    </td>
                                    <td><?php echo $val->sequence_no; ?></td>
                                    <td><?php echo date('dS M Y ', strtotime($val->created_at)); ?></td>
                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" class="chk_status" value="<?php echo $val->id;?>" <?php echo ($val->status) ? 'checked' : '';?>>
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td><a href="<?php echo base_url(); ?>index.php/categories/addsubcategory/addbypaentcat/<?php echo $val->id; ?>">Add a sub-category</a></td>
                                    <td>
                                        <a href="<?php echo base_url().'index.php/categories/add/'.$val->id.'';?>">EDIT</a>
                                        &nbsp;
                                        <a href="javascript: void(0)" onClick="delete_category('category', <?php echo $val->id; ?>)">DELETE</a>
                                    </td>
                                </tr><?php
                            }
                            ?></tbody>
                        </table>
                    </div>
                </div>
            </div>
         </div>

        <?php
        if (isset($links)) {
            ?><div class="row"><div class="col-md-12"><?php
            echo $links;
            ?></div></div><?php
        }
        ?>
    </div>
</div>
<script>
function toggleStatus(table_name, id, column_name, status, $this){
    var status_text = (status == 1) ? 'activate':'deactivate';
    if(confirm('Are you sure, you want to '+status_text+' this category?')) {
        jQuery.ajax({
            type:'POST',
            data:{table_name, id, column_name, status},
            url:'<?php echo base_url(); ?>index.php/common/toggle_status',
            dataType: 'JSON'

        }).done(function(data){
            if(!data.error) {
                alert(data.message);
            } else {
                alert('Something went wrong, please try after sometime');
                window.location.reload();
            }

        }).fail(function (jqXHR, textStatus){
            alert('Something went wrong, please try after sometime');
            window.location.reload();
        });
    } else {
        $this.prop('checked', !$this.is(':checked'));
    }
}

function delete_category(table_name, id) {
    var table_name = $.trim(table_name);
    var id = $.trim(id);

    if(table_name != '' && id != '') {
        if(confirm('Are you sure, you want to delete this category?')) {
            jQuery.ajax({
                type:'POST',
                data:{table_name, id},
                url:'<?php echo base_url(); ?>index.php/common/delete'
            }).done(function(data){
                rawdata=jQuery.parseJSON(data);
                if(rawdata.error==false){
                    jQuery('.row_'+id).fadeOut('slow');
                }
            });
        }
    }
}

/*
function deleteadmincategories(module, id, prefix=false){
    if(confirm('Are you sure, you want to delete this category?')) {
        jQuery.ajax({
            type:'POST',
            data:{module, id, prefix},
            url:'<?php echo base_url(); ?>index.php/common/delete'
        }).done(function(data){
            rawdata=jQuery.parseJSON(data);
            if(rawdata.error==false){
                jQuery('.row_'+id).fadeOut('slow');
            }
        });
    }
}
*/

$(document).ready(function(){
    $('.chk_status').click(function(){
        var table_name = 'category';
        var id = $(this).val();
        var column_name = 'status';
        var status = ($(this).is(':checked')) ? 1 : 0;
        var $this = $(this);
        toggleStatus(table_name, id, column_name, status, $this);
    });

    $(".submit_btn").on("click", function(e){
        e.preventDefault();
        $('#categoryForm').attr('action', "<?php echo base_url('index.php/categories');?>").submit();
    });

    $(".download_report").on("click", function(e){
        $('#categoryForm').attr('action', "<?php echo base_url('index.php/categories/exportExcel');?>").submit();
    });
});
</script>