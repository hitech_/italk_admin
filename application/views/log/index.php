<div class="content logs-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header logs-head error_class">
                        <h3 class="text-center">Logs Report</h3>
                        <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <?php echo form_open('logs', array('id' => 'logsForm', 'method'=> 'get')); ?>
                                <input type="hidden" name="fromdate">
                                <input type="hidden" name="todate">
                                <input type="hidden" name="disabilitytypetext" value="<?php echo $param['disabilitytypetext'];?>">
                                <input type="hidden" name="durationtext" value="<?php echo $param['durationtext'];?>">

                                <div class="form-group">
                                <label for="disabilitytype">Disability Type</label>
                                    <select name="disabilitytype" id="disabilitytype" class="form-control">
                                        <option value="1" <?php echo ($param['disabilitytype'] == 1) ? 'selected' : '';?>>Normal</option>
                                        <option value="2" <?php echo ($param['disabilitytype'] == 2) ? 'selected' : '';?>>Autism</option>
                                        <option value="3" <?php echo ($param['disabilitytype'] == 3) ? 'selected' : '';?>>Blind</option>
                                        <option value="4" <?php echo ($param['disabilitytype'] == 4) ? 'selected' : '';?>>Handicap</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="filterBy">Filter By</label>
                                    <select name="duration" id="filterBy" class="form-control">
                                        <option value="1" <?php echo ($param['duration'] == 1) ? 'selected' : '';?>>Daily</option>
                                        <option value="2" <?php echo ($param['duration'] == 2) ? 'selected' : '';?>>Weekly</option>
                                        <option value="3" <?php echo ($param['duration'] == 3) ? 'selected' : '';?>>Monthly</option>
                                        <option value="4" <?php echo ($param['duration'] == 4) ? 'selected' : '';?>>Date Range</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-info btn-fill">Search</button>
                            <?php echo form_close(); ?>
                        </div>
                        </div>
                    </div>
                    <div class="filter-by">
                        <p>Disability Type: <span id="disabilityTypeText"><?php echo !empty($param['disabilitytypetext']) ? $param['disabilitytypetext'] : '-';?></span></p>
                        <p>Filter by: <span id="filterByText"><?php echo !empty($param['durationtext']) ? $param['durationtext'] : '-';?></span></p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Sr#</th>
                                    <th>Username</th>
                                    <th>Disability Type</th>
                                    <th>Login Time</th>
                                    <th>Logout Time</th>
                                    <th>Total Time Spent</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody><?php
                            if(!empty($logs)){
                                $i=0;
                                //echo '<pre>'; print_r($logs); exit;
                                foreach($logs as $key=>$val){
                                    $i++;

                                    $last_login_seconds = !empty($val->last_login_date) ? strtotime($val->last_login_date) : 0;
                                    $last_logout_seconds = !empty($val->last_logout_time) ? strtotime($val->last_logout_time) : 0;
                                    $timed_spent = ($last_logout_seconds > $last_login_seconds) ? ($last_logout_seconds - $last_login_seconds) : 0;
                                    ?><tr>
                                        <td><?php echo $i;?></td>
                                        <td><?php echo !empty($val->u_username) ? $val->u_username : '-'; ?></td>
                                        <td><?php echo !empty($val->u_disability_type) ? $val->u_disability_type : '-'; ?></td>
                                        <td><?php echo !empty($val->last_login_date) ? $val->last_login_date : '-'; ?></td>
                                        <td><?php echo ($last_logout_seconds > $last_login_seconds) ? $val->last_login_date: '-'; ?></td>
                                        <td><?php echo !empty($timed_spent) ? convert_sec_to_day_time_format($timed_spent) : '-';?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>index.php/logs/view<?php echo $query_string.'&user_id='.$val->u_user_id;?>" class="btn btn-info btn-fill">Details</a>
                                        </td>
                                    </tr><?php
                                }
                            }
                            ?></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if (isset($links)) {
            ?><div class="row"><div class="col-md-12"><?php
                echo $links;
                ?></div></div><?php
        }
        ?>
    </div>
</div>

<div id="flashmodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p><?php echo $this->session->flashdata('flashmessage'); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closemodalfooter" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div><?php
function convert_sec_to_day_time_format($seconds) {
    $ret = "";

    /*** get the days ***/
    $days = intval(intval($seconds) / (3600 * 24));
    if ($days > 0) {
        $ret .= "$days Days ";
    }

    /*** get the hours ***/
    $hours = (intval($seconds) / 3600) % 24;
    if ($hours > 0) {
        $ret .= "$hours Hrs ";
    }

    /*** get the minutes ***/
    $minutes = (intval($seconds) / 60) % 60;
    if ($minutes > 0) {
        $ret .= "$minutes Mins ";
    }

    /*** get the seconds ***/
    $seconds = intval($seconds) % 60;
    if ($seconds > 0) {
        //$ret .= "$seconds seconds";
    }

    return $ret;
}
?><script>
$(document).ready(function() {

    $("#disabilitytype").change(function() {
        var disabilitytypetext = $('#disabilitytype option:selected').text()
        $('#disabilityTypeText').html(disabilitytypetext);
        $('input[name="disabilitytypetext"]').val(disabilitytypetext);
    });

    $("#filterBy").change(function() {
       if($(this).val() == '4'){
          $('#myModal1').modal('show')
       } else {
           var durationtext = $('#filterBy option:selected').text();
           $('#filterByText').html(durationtext);
           $('input[name="durationtext"]').val(durationtext);
       }
    });

    $('[data-toggle="datepicker"]').datepicker({
        autoHide: true,
        format: 'dd-mm-yyyy'
    });
});

jQuery('#storedates').on('click', function(){
    var start_date = $('input[name=selectfromdate]').val();
    var end_date = $('input[name=selecttodate]').val();

    var durationtext = start_date+' to '+end_date;
    $('#filterByText').html(durationtext);
    $('input[name="durationtext"]').val(durationtext);

    jQuery('input[name=fromdate]').val(jQuery('input[name=selectfromdate]').val());
    jQuery('input[name=todate]').val(jQuery('input[name=selecttodate]').val());
    jQuery('#myModal1').modal('hide');
})
</script>
