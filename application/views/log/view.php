<?php 
// print_r($logs);die;
?>

<div class="content logs-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <!-- <div class="header logs-head error_class">
                        <h3 class="text-center">Logs Report</h3>
                        <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <form>
                              <div class="form-group">
                                <label for="">Disability Type</label>
                                <select name="" id="" class="form-control">
                                    <option value="1">Normal</option>
                                    <option value="2">Autism</option>
                                    <option value="3">Blind</option>
                                    <option value="4">Handicap</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="">Filter By</label>
                                <select name="" id="filterBy" class="form-control">
                                    <option value="1">Daily</option>
                                    <option value="2">Weekly</option>
                                    <option value="3">Monthly</option>
                                    <option value="4">From Date to Date</option>
                                </select>
                              </div>
                              <button type="submit" class="btn btn-info btn-fill">Search</button>
                            </form>
                        </div>
                        </div>
                    </div>
                    <div class="filter-by"><p>Filter by: <span>22 Jan 2018</span> to <span>25 Feb 2018</span></p></div> -->

                    <div class="header logs-head error_class">
                        <h3 class="text-center">Logs Report (<?php echo !empty($user_detail[0]->u_username) ? $user_detail[0]->u_username : '-';?>)</h3>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <form>
                                    <div class="form-group">
                                        <label for="">Email Id:</label>
                                        <span><?php echo !empty($user_detail[0]->u_parent_email_id) ? $user_detail[0]->u_parent_email_id : '-';?></span>
                                    </div>
                                    <button id="email_report" type="button" class="btn btn-info btn-fill">Email Report</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="filter-by"><p>Filter by: <span><?php echo $filter['durationtext'];?></span></p></div>

                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Sr#</th>
                                    <th>Category</th>
                                    <th>Sub category</th>
                                    <th>Sentence</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($logs)){
                                    $sr_no = $offset;
                                    foreach ($logs as $key => $value) {
                                        $sr_no++;
                                        $c_name = ($value->lang_type == 'en') ? $value->c_name : $value->c_name_ar;
                                        $s_name = ($value->lang_type == 'en') ? $value->s_name : $value->s_name_ar;
                                        $sentence = ($value->lang_type == 'en') ? $value->sentence : $value->sentence_ar;
                                        ?><tr>
                                            <td><?php echo $sr_no ?></td>
                                            <td><?php echo $c_name?></td>
                                            <td><?php echo $s_name?></td>
                                            <td><?php echo $sentence?></td>
                                            <td><?php echo $value->created_at?></td>
                                            <td><?php echo ($value->success == '1') ? '<a href="javascript:void(0);" class="btn btn-info btn-fill">Success</a>' : '<a href="javascript:;" class="btn btn-danger btn-fill">Failure</a>' ?></td>
                                        </tr>
                                        <?php
                                    }
                                }else{
                                    ?>
                                     <tr>
                                        <td>No Data Found</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if (isset($links)) {
            ?><div class="row"><div class="col-md-12"><?php
                echo $links;
                ?></div></div><?php
        }
        ?>
    </div>
</div>

<div id="flashmodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p><?php echo $this->session->flashdata('flashmessage'); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closemodalfooter" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
$( document ).ready(function() {
//    $( "#filterBy" ).change(function() {
//      if($(this).val() == '4'){
//        $('#myModal1').modal('show')
//      }
//    });
//    $('[data-toggle="datepicker"]').datepicker({
//        autoHide: true,
//        format: 'dd/mm/yyyy'
//    });

    $('#email_report').on('click', function(){
        if(confirm('Are you sure, you want to email the logs report?')) {
            alert('Work In Progress');
        }
    });
});
</script>
