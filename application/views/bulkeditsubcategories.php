<style type="text/css">
    .dataTables_length{
        display: none;
    }
    .dataTables_filter{
        display: none;}
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header pagetitle">
                        <h4 class="title"><?php echo $pagetitle; ?></h4>
                        <!--<a href="<?php echo base_url(); ?>index.php/categories/addsubcategory">Add a Subcategory</a>-->
                    </div>
                    <div class="col-md-12 search-wrap">
                        <?php echo form_open('categories/bulkeditsubcategory', array('id' => 'subcategoryForm', 'method'=> 'get')); ?>
                        <div class="">
                            <label>Search By Category / Subcategory</label>
                        </div>
                        <div class="">
                            <select name="category" id="category" class="form-control">
                            <option value="">Select Category</option><?php
                            foreach ($categories as $c_key => $c_value) {
                                ?><option value="<?php echo $c_value->id ?>" <?php echo ($filter['category'] == $c_value->id) ? 'selected' : '';?>><?php echo $c_value->eng_name?></option><?php
                            }
                            ?></select>
                        </div>

                        <div class="">
                            <select name="subcategory" id="subcategory" class="form-control">
                                <option value="">Select Subcategory</option><?php
                                if(!empty($subcategories)) {
                                    foreach ($subcategories as $sc_key => $sc_value) {
                                        ?><option value="<?php echo $sc_value->id ?>" <?php echo ($filter['subcategory'] == $sc_value->id) ? 'selected' : '';?>><?php echo $sc_value->eng_name?></option><?php
                                    }
                                }
                            ?></select>
                        </div>

                        <button type="submit" class="btn btn-info btn-fill submit_btn">Search</button>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <tr>
                                <td><?php
                                    if(!empty($filter['subcategory'])) {
                                        ?><div class="header error_class">
                                            <p style="color:red"><?php echo validation_errors()?></p>
                                            <p><?php //echo $message; ?></p>
                                        </div>

                                        <div class="content flashmessage" data-flashdata="<?php echo $this->session->flashdata('flashmessage'); ?>" data-error=<?php echo $error; ?>>
                                        <form class="categoryform" method="POST" action="?category=<?php echo $filter['category'];?>&subcategory=<?php echo $filter['subcategory'];?>" enctype="multipart/form-data" accept-charset="utf-8">
                                            <input type="hidden" name="category" id="category" value="<?php echo $filter['category'];?>">
                                            <input type="hidden" name="subcategory" id="subcategory" value="<?php echo $filter['subcategory'];?>">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label>Sequence No</label>
                                                        <input type="text" class="form-control" placeholder="Sequence No" name="sequence_no" value="<?php echo $existingsubcatdata['sequence_no']; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-5 <?php echo ($existingsubcatdata['sc_ar_audio']) ? 'displaynoneclass' : ''; ?>">
                                                    <div class="form-group">
                                                        <label for="categorystatus">Arabic Audio</label>
                                                        <input type="file" class="form-control" placeholder="Please select an arabic audio" name="sc_ar_audio" value="">
                                                    </div>
                                                </div>
                                                <?php if($existingsubcatdata['sc_ar_audio']){ ?>
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label for="categorystatus">Existing Arabic Audio</label><br>
                                                            <audio controls src="data:audio/ogg;base64,<?php echo $existingsubcatdata['sc_ar_audio'];?>" />
                                                        </div>
                                                        <div style="margin-top: -20px;"><a href="javascript:void(0);" onclick="delete_audio('sub_category','sc_ar_audio','<?php echo $existingsubcatdata['id'];?>')">Delete</a></div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label>Subcategory Name (English)</label>
                                                        <input type="text" class="form-control" placeholder="Subcategory Name (English)" name="sub_eng_name" value="<?php echo $existingsubcatdata['engsubcatname']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label>Subcategory Name (Arabic)</label>
                                                        <input type="text" class="form-control" placeholder="Subcategory Name (Arabic)" name="sub_ar_name" value="<?php echo $existingsubcatdata['arsubcatname']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label>Subcategory Name (English-Arabic)</label>
                                                        <input type="text" class="form-control" placeholder="Subcategory Name (English-Arabic)" name="sub_eng_ar_name" value="<?php echo $existingsubcatdata['eng_ar_name'];?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label>Subcategory Name (Arabic-English)</label>
                                                        <input type="text" class="form-control" placeholder="Subcategory Name (Arabic-English)" name="sub_ar_eng_name" value="<?php echo $existingsubcatdata['ar_eng_name']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label>Select parent Category</label>
                                                        <select class="form-control" name="parent_cat" id="parent_cat">
                                                            <option value="">Select Parent Category</option><?php
                                                            foreach ($categories as $c_key => $c_value) {
                                                            ?><option value="<?php echo $c_value->id ?>" <?php echo ($existingsubcatdata['parentcatid'] == $c_value->id) ? 'selected' : '';?>><?php echo $c_value->eng_name?></option><?php
                                                            }
                                                        ?></select>
                                                    </div>
                                                </div>
                                                <div class="col-md-5 <?php echo ($existingsubcatdata) ? 'displaynoneclass' : ''; ?>">
                                                    <div class="form-group">
                                                        <label for="categorystatus">Image</label>
                                                        <input type="file" class="form-control" placeholder="Image" name="sub_image" value="">
                                                    </div>
                                                </div>
                                            </div>

                                            <?php if($existingsubcatdata){ ?>
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <label for="categorystatus">Existing Image</label><br>
                                                        <img src="data:image/jpeg;base64, <?php echo $existingsubcatdata['sc_image']; ?>" width="100" height="100" class="editedimage">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <p class="filename"></p>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                            <?php if($existingsubcatdata['sentence']){
                                                $i=0;
                                                foreach($existingsubcatdata['sentence'] as $key=>$val){
                                                    $j=$i+1;
                                                    ?>
                                                    <div class="row mainele mainele_<?php echo $j; ?>">
                                                        <div class="col-md-10" style="padding-bottom: 0">
                                                            <input type="hidden" name="hdSentenceId[]" value="<?php echo $val['sentence_id'];?>">
                                                            <label class="col-md-3"  style="padding-bottom: 0;padding-left: 0" for="categorystatus">English Word</label>
                                                            <label class="col-md-3"  style="padding-bottom: 0;padding-left: 0" for="categorystatus">Arabic Word</label>
                                                            <label class="col-md-3"  style="padding-bottom: 0;padding-left: 0" for="categorystatus">Arabic-English Word</label>
                                                            <label class="col-md-3"  style="padding-bottom: 0;padding-left: 0" for="categorystatus">English-Arabic Word</label>
                                                        </div>
                                                        <div class="col-md-10"  style="padding: 0">
                                                            <!-- <label for="categorystatus">Sentence <?php //echo $j; ?></label><br> -->
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control sentences" placeholder="Add English Word" name="englishsentence[]" id="englishsentence_<?php echo $j; ?>" value="<?php echo $val['sentence_en']; ?>">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control sentences" placeholder="Add Arabic Word" name="arabicsentence[]" id="arabicsentence_<?php echo $j; ?>" value="<?php echo $val['sentence_ar']; ?>">
                                                                <input type="file" class="form-control" placeholder="Image" name="arabic_audio[]" value="" style="display: none;">
                                                                <input type="button" name="arabic_audio_btn[]" value="Add Ar Audio" style="display:<?php echo (!empty($val['sentence_ar_audio'])) ? 'none' : '';?>">
                                                                <p class="sentence_filename" style="display:<?php echo (!empty($val['sentence_ar_audio'])) ? 'none' : '';?>"></p>
                                                                <?php
                                                                if(!empty($val['sentence_ar_audio'])) {
                                                                    ?><input type="button" name="arabic_audio_play[]" value="Play" data-audio="<?php echo $val['sentence_ar_audio'];?>">
                                                                    <span><a href="javascript:void(0);" onclick="delete_audio('sentences','ar_audio','<?php echo $val['sentence_id'];?>')">Delete</a></span><?php
                                                                }
                                                                ?></div>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control sentences" placeholder="Add Arabic-English Word" name="arabicenglishsentence[]" id="arabicenglishsentence_<?php echo $j; ?>" value="<?php echo $val['sentence_ar_en']; ?>">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control sentences" placeholder="Add English-Arabic Word" name="englisharabicsentence[]" id="englisharabicsentence_<?php echo $j; ?>" value="<?php echo $val['sentence_en_ar']; ?>">
                                                            </div>
                                                            <!--<div class="col-md-2">
                                                <button type="button" class="btn btn-info btn-fill removesentense" onClick="removesentense(this, <?php echo $j; ?>)">-</button>
                                            </div>-->
                                                            <?php if($j==count($existingsubcatdata['sentence'])){ ?>
                                                                <div class="col-md-1">
                                                                    <button type="button" class="btn btn-info btn-fill addmoresentences">+</button>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php $i++; } }else{ ?>
                                                <div class="row mainele mainele_1">
                                                    <div class="col-md-10">
                                                        <!-- <label for="categorystatus">Sentence 1</label><br> -->
                                                        <div class="col-md-3">
                                                            <label for="categorystatus">English Word</label>
                                                            <input type="text" class="form-control sentences" placeholder="Add English Word" name="englishsentence[]" id="englishsentence_1">
                                                         </div>
                                                        <div class="col-md-3">
                                                            <label for="categorystatus">Arabic Word</label>
                                                            <input type="text" class="form-control sentences" placeholder="Add Arabic Word" name="arabicsentence[]" id="arabicsentence_1">
                                                            <input type="file" class="form-control" placeholder="Image" name="arabic_audio[]" value="" style="display: none">
                                                            <input type="button" name="arabic_audio_btn[]" value="Add Ar Audio">
                                                            <p class="sentence_filename"></p>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for="categorystatus">Arabic-English Word</label>
                                                            <input type="text" class="form-control sentences" placeholder="Add Arabic-English Word" name="arabicenglishsentence[]" id="arabicenglishsentence_1">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for="categorystatus">English-Arabic Word</label>
                                                            <input type="text" class="form-control sentences" placeholder="Add English-Arabic Word" name="englisharabicsentence[]" id="englisharabicsentence_1">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <button type="button" class="btn btn-info btn-fill addmoresentences">+</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <button type="submit" class="btn btn-info btn-fill pull-right">Update Subcategory</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div><?php
                                    } else {
                                        ?><div>Note:- Please select Category and Subcateogry to update</div><?php
                                    }
                                ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="flashmodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p><?php echo $this->session->flashdata('flashmessage'); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closemodalfooter" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $('#category').on('change', function () {
        var category = $.trim($(this).val());
        if(category != '') {
            $.ajax({
                url:'<?php echo base_url(); ?>index.php/api/get_subcategories',
                data:{category:category},
                type:'POST',
                dataType:'JSON',
                beforeSend: function() {
                    $('#subcategory').html($('<option>').text('Loading Subcategory...').attr('value', ''));
                }
            }).done(function(data){
                if(data.status === 'success'){
                    $('#subcategory').html($('<option>').text('Select Subcategory').attr('value', ''));
                    $.each(data.data, function(index, value) {
                        $('#subcategory').append($('<option>').text(value.eng_name).attr('value', value.id));
                    });
                }
            }).error(function() {
                $('#subcategory').html($('<option>').text('Select Subcategory').attr('value', ''));
                alert('Error occurred, please try again sometime');
            });
        } else {
            $('#subcategory').html($('<option>').text('Select Subcategory').attr('value', ''));
        }
    });
});

var cursentencecount=jQuery('.mainele').length;
var nextsentencecount=cursentencecount + 1;
var error=jQuery('.flashmessage').data('error');

jQuery('.addmoresentences').on('click', function(){
    var element='<div class="row mainele_'+nextsentencecount+'"><div class="col-md-10"><br><div class="col-md-3"><label for="categorystatus">English Sentence '+nextsentencecount+'</label><input type="text" class="form-control sentences" placeholder="Add English Sentence" name="englishsentence[]" id="englishsentence_'+nextsentencecount+'"></div><div class="col-md-3"> <label for="categorystatus">Arabic Sentence '+nextsentencecount+'</label><input type="text" class="form-control sentences" placeholder="Add Arabic Sentence" name="arabicsentence[]" id="arabicsentence_'+nextsentencecount+'"><input type="file" class="form-control" placeholder="Image" name="arabic_audio[]" value="" style="display: none"><input type="button" name="arabic_audio_btn[]" value="Add Ar Audio"><p class="sentence_filename"></p></div> <div class="col-md-3"><label for="categorystatus">Arabic-English Sentence '+nextsentencecount+'</label><input type="text" class="form-control sentences" placeholder="Add Arabic-English Sentence" name="arabicenglishsentence[]" id="arabicenglishsentence_'+nextsentencecount+'"></div> <div class="col-md-3"><label for="categorystatus">English-Arabic Sentence '+nextsentencecount+'</label><input type="text" class="form-control sentences" placeholder="Add English-Arabic Sentence" name="englisharabicsentence[]" id="englisharabicsentence_'+nextsentencecount+'"></div> <div class="col-md-2"><button type="button" class="btn btn-info btn-fill removesentense" onClick="removesentense(this, '+nextsentencecount+')">-</button></div> </div> </div>';
    jQuery('.mainele_'+cursentencecount).after(element);
    cursentencecount++;
    nextsentencecount++;
});

function removesentense(ele, counter, deleteid=false){
    jQuery('.mainele_'+counter).remove();
    cursentencecount--;
    nextsentencecount--;
}

jQuery(document).ready(function(){
    var flashdata=jQuery('.flashmessage').data('flashdata');
    console.log(flashdata);
    if(flashdata){
        jQuery('#flashmodal').modal('show');
    }
})

jQuery('.close, .closemodalfooter').on('click', function(){
    if(!error){
        // window.location.href='<?php echo base_url(); ?>index.php/categories/subcategories';
        var current_url =window.location.href;
        window.location.href=current_url;
    }
})

jQuery('.editedimage').on('mouseover', function(){
    jQuery(this).css({'opacity':'0.2'});
    jQuery(this).next('i').show();
    jQuery(this).addClass('editnow');
})

jQuery('.editedimage').on('mouseout', function(){
    jQuery(this).css({'opacity':''});
    jQuery(this).next('i').hide();
    jQuery(this).removeClass('editnow');
})

jQuery('.editedimage, .fa-pencil-square-o').on('click', function(){
    if(jQuery(this).hasClass('editnow')){
        jQuery('input[name=sub_image]').click();
    }
})

jQuery('input[name=sub_image]').on('change', function(){
    jQuery('.filename').html('');
    filename=jQuery(this).val();
    jQuery('.filename').append(filename);
});

function delete_audio(table_name, column_name, id) {
    var table_name = $.trim(table_name);
    var column_name = $.trim(column_name);
    var id = $.trim(id);

    if(table_name != '' && column_name != '' && id != '') {
        if(confirm('Are you sure, you want to delete this audio file?')) {
            jQuery.ajax({
                type:'POST',
                data:{table_name, column_name, id},
                url:'<?php echo base_url(); ?>index.php/common/delete_audio'
            }).done(function(data){
                rawdata=jQuery.parseJSON(data);
                if(rawdata.error==false){
                    $('#flashmodal .modal-body p').text(rawdata.message);
                    jQuery('#flashmodal').modal('show');
                }
            });
        }
    }
}

//jquyery
//jquyery
$(document).on('click', 'input[name="arabic_audio_btn[]"]', function () {
    var index = $('input[name="arabic_audio_btn[]"]').index(this);
    $('input[name="arabic_audio[]"]').eq(index).click();
});

$(document).on('change', 'input[name="arabic_audio[]"]', function () {
    var index = $('input[name="arabic_audio[]"]').index(this);
    $('.sentence_filename').eq(index).html(jQuery(this).val());
});

jQuery('input[name="arabic_audio_play[]"]').on('click', function(){
    var audio = new Audio("data:audio/ogg;base64," + $(this).attr('data-audio'));
    audio.play();
});

</script>