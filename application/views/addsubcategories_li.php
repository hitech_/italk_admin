

<div class="content logs-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header logs-head error_class">
                        <h3 class="text-center">Logs Report</h3>
                        <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <form>
                              <div class="form-group">
                                <label for="">Disability Type</label>
                                <select name="" id="" class="form-control">
                                    <option value="1">Normal</option>
                                    <option value="2">Autism</option>
                                    <option value="3">Blind</option>
                                    <option value="4">Handicap</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="">Filter By</label>
                                <select name="" id="filterBy" class="form-control">
                                    <option value="1">Daily</option>
                                    <option value="2">Weekly</option>
                                    <option value="3">Monthly</option>
                                    <option value="4">From Date to Date</option>
                                </select>
                              </div>
                              <button type="submit" class="btn btn-info btn-fill">Search</button>
                            </form>
                        </div>
                        </div>
                    </div>
                    <div class="filter-by"><p>Filter by: <span>22 Jan 2018</span> to <span>25 Feb 2018</span></p></div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Sr#</th>
                                    <th>Username</th>
                                    <th>Total Spend Time</th>
                                    <th>Login Time</th>
                                    <th>Logout Time</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Azeem</td>
                                    <td>3:15</td>
                                    <td>1:17 am</td>
                                    <td>2:20 am</td>
                                    <td><a href="javascript:;" class="btn btn-info btn-fill">Details</a></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>John Doe</td>
                                    <td>12:15</td>
                                    <td>4:17 am</td>
                                    <td>6:20 am</td>
                                    <td><a href="javascript:;" class="btn btn-info btn-fill">Details</a></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Mark Doe</td>
                                    <td>1:15</td>
                                    <td>6:17 am</td>
                                    <td>5:50 am</td>
                                    <td><a href="javascript:;" class="btn btn-info btn-fill">Details</a></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>John Doe</td>
                                    <td>2:56</td>
                                    <td>4:12 am</td>
                                    <td>7:47 am</td>
                                    <td><a href="javascript:;" class="btn btn-info btn-fill">Details</a></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Mark Doe</td>
                                    <td>9:15 pm</td>
                                    <td>10:17 am</td>
                                    <td>1:29 am</td>
                                    <td><a href="javascript:;" class="btn btn-info btn-fill">Details</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="flashmodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p><?php echo $this->session->flashdata('flashmessage'); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closemodalfooter" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
$( document ).ready(function() {
    $( "#filterBy" ).change(function() {
      if($(this).val() == '4'){
        $('#myModal1').modal('show')
      }      
    });
    $('[data-toggle="datepicker"]').datepicker({
        autoHide: true,
        format: 'dd/mm/yyyy'
    });
});
</script>
