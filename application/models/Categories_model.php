<?php
class Categories_model extends CI_Model {
    function __construct() {
    	parent::__construct();
    }

	//$order_by = ['sequence_no' => 'ASC']
	public function get_categories($where, $limit=0, $offset=0, $order_by) {
		//status != -1 (get non deleted records)
		$this->db->where('status !=', -1);

		if(!empty($where['category_name'])) {
			$this->db->like('eng_name', $where['category_name']);
		}

		if(!empty($where['id'])) {
			$this->db->where('id', $where['id']);
		}

		if(!empty($order_by)) {
			foreach($order_by as $k => $v) {
				$v = !empty($v) ? $v : 'ASC';
				$this->db->order_by($k, $v);
			}
		}

		$this->db->limit($limit, $offset);
		$result = $this->db->get("category")->result();
		if (!empty($result)) {
			return $result;
		}

		return false;
	}

	public function get_total_categories($where) {
		$this->db->where('status !=', -1);

		if(!empty($where['category_name'])) {
			$this->db->like('eng_name', $where['category_name']);
		}

		if(!empty($where['id'])) {
			$this->db->where('id', $where['id']);
		}

		$this->db->from('category');
		$count = $this->db->count_all_results();
		return $count;
	}

	public function get_subcategories($where, $limit=0, $offset=0, $order_by='', $fields="")
	{
		//$this->db->where('type', 'Original');

//		$wherecond="";
//		$extracolumns="";
//		$extrajoins="";
//		if($param['parentcatid']){
//			$wherecond.=" AND c.id=".$param['parentcatid']."";
//		}
//		if($param['subcatid']){
//			$wherecond.=" AND s.id=".$param['subcatid']."";
//			$extracolumns=" , se.id sentenseid, se.sentence, se.language_type ";
//			$extrajoins=" LEFT JOIN sentences se ON s.id=se.sub_category_id ";
//		}
//
		$this->db->where('C.status !=', -1);
		$this->db->where('S.status !=', -1);

		if (!empty($where['category_id'])) {
			$this->db->where('category_id', $where['category_id']);
		}

		$this->db->limit($limit, $offset);

		$fields = !empty($fields) ? $fields : '
			S.id, S.eng_name engsubcatname, S.ar_name arsubcatname, S.eng_ar_name, S.ar_eng_name, S.category_id parentcatid, S.sc_image, S.status, S.sequence_no, S.created_at,
			C.eng_name engparentcatname, C.ar_name arparentcatname';

		$this->db->select($fields)
			->from('sub_category as S')
			->join('category as C', 'S.category_id = C.id');

		if(!empty($order_by)) {
			foreach($order_by as $k => $v) {
				$v = !empty($v) ? $v : 'ASC';
				$this->db->order_by($k, $v);
			}
		}
		$data = $this->db->get()->result();

		if (!empty($data)) {
			return $data;
		}

		return false;
	}

	public function get_total_subcategories($where) {
		$this->db->where('status !=', -1);
		//echo '<pre>'; print_r($where); exit;
		if(!empty($where['category_id'])) {
			$this->db->where('category_id', $where['category_id']);
		}

		$this->db->from('sub_category');
		$count = $this->db->count_all_results();

		return $count;
	}

	public function add($param, $catid=false){
		if($catid){
			$this->db->where('id', $catid);
			return $this->db->update($this->categorytablename, $param);
		}
		return $this->db->insert($this->categorytablename, $param);
	}

	public function subcategories($param=false){
		$wherecond="";
		$extracolumns="";
		$extrajoins="";
		if($param['parentcatid']){
			$wherecond.=" AND c.id=".$param['parentcatid']."";
		}

		if($param['subcatid']){
			$wherecond.=" AND s.id=".$param['subcatid']."";
			$extracolumns=" , se.id sentence_id, se.en sentence_en, se.ar sentence_ar, se.en_ar sentence_en_ar, se.ar_en sentence_ar_en, se.ar_audio sentence_ar_audio, se.status ";
			$extrajoins=" LEFT JOIN sentences se ON s.id=se.sub_category_id ";
		}
		$select="SELECT s.id, s.sequence_no, s.eng_name engsubcatname, s.ar_name arsubcatname, s.category_id parentcatid, s.sc_image, s.sc_ar_audio, c.eng_name engparentcatname, c.ar_name arparentcatname, s.created_at, s.eng_ar_name, s.ar_eng_name ".$extracolumns." FROM sub_category s LEFT JOIN category c ON s.category_id=c.id ".$extrajoins." WHERE 1 ".$wherecond."";
		$res=$this->db->query($select);
		return $res->result();
	}

	public function addsubcategory($param, $sentenceparam, $subcatid=false){
		$returnparam=false;
		if($subcatid){
			$this->db->where('id', $subcatid);
			if(isset($param['created_at'])) {
				unset($param['created_at']);
			}
			$res=$this->db->update($this->subcategorytablename, $param);
		}else{
			$res=$this->db->insert($this->subcategorytablename, $param);
			$lastrecordid=$this->db->insert_id();
		}
		$subcategoryid=($subcatid) ? $subcatid : $lastrecordid;
		if($subcategoryid) {
			//$selectpreviousentries="SELECT id FROM sentences WHERE sub_category_id=".$subcategoryid."";
			//$resselectpreviousentries=$this->db->query($selectpreviousentries);
			//$resultsprevious=$resselectpreviousentries->result_array();

			//if($resselectpreviousentries->num_rows() > 0){
			//foreach($resultsprevious as $delkey=>$delval){
			//$deleteinsert="INSERT INTO deleted_list SET deleted_id=".$delval['id'].",  deleted_type='3', updated_at='".$param['updated_at']."', created_at='".$param['created_at']."'";
			//$resdeleteinsert=$this->db->query($deleteinsert);
			//}

			//$deleteprevious="DELETE FROM sentences WHERE sub_category_id=".$subcategoryid."";
			//$resdeleteprevious=$this->db->query($deleteprevious);
			//}

			$arabicsentences = !empty($sentenceparam['arabicsentence']) ? $sentenceparam['arabicsentence'] : [];
			$englishsentences = !empty($sentenceparam['englishsentence']) ? $sentenceparam['englishsentence'] : [];
			$englisharabicsentences = !empty($sentenceparam['englisharabicsentence']) ? $sentenceparam['englisharabicsentence'] : [];
			$arabicenglishsentences = !empty($sentenceparam['arabicenglishsentence']) ? $sentenceparam['arabicenglishsentence'] : [];
			$arabic_audios = !empty($sentenceparam['arabic_audio']['tmp_name']) ? $sentenceparam['arabic_audio']['tmp_name'] : [];
			$sentence_ids = !empty($sentenceparam['sentence_id']) ? $sentenceparam['sentence_id'] : [];

			if (!empty($englishsentences)) {
				foreach ($englishsentences as $k => $v) {
					$sentence_id = !empty($sentence_ids[$k]) ? $sentence_ids[$k] : '';

					$englishsentence = !empty($englishsentences[$k]) ? $englishsentences[$k] : '';
					$arabicsentence = !empty($arabicsentences[$k]) ? $arabicsentences[$k] : '';
					$englisharabicsentence = !empty($englisharabicsentences[$k]) ? $englisharabicsentences[$k] : '';
					$arabicenglishsentence = !empty($arabicenglishsentences[$k]) ? $arabicenglishsentences[$k] : '';
					$arabic_audio = !empty($arabic_audios[$k]) ? base64_encode(file_get_contents($arabic_audios[$k])) : '';

					if (!empty($sentence_id)) {
						$update = 'UPDATE sentences SET ' .
							'sub_category_id=\'' . $subcategoryid . '\',' .
							'category_id=\'' . $param['category_id'] . '\',' .
							'en=\'' . $englishsentence . '\',' .
							'ar=\'' . $arabicsentence . '\',' .
							'en_ar=\'' . $englisharabicsentence . '\',' .
							'ar_en=\'' . $arabicenglishsentence . '\',';

						if (!empty($arabic_audio)) {
							$update .= ' ar_audio=\'' . $arabic_audio . '\',';
						}

						$update .= ' updated_at=\'' . $param['updated_at'] . '\''.
								   ' WHERE id=' . $sentence_id;
						$update_res = $this->db->query($update);
						//echo $update."<br>";exit;
					} else {
						$insert = 'INSERT INTO sentences SET ' .
							'sub_category_id=\'' . $subcategoryid . '\',' .
							'category_id=\'' . $param['category_id'] . '\',' .
							'en=\'' . $englishsentence . '\',' .
							'ar=\'' . $arabicsentence . '\',' .
							'en_ar=\'' . $englisharabicsentence . '\',' .
							'ar_en=\'' . $arabicenglishsentence . '\',' .
							'ar_audio=\'' . $arabic_audio . '\',' .
							'updated_at=\'' . $param['updated_at'] . '\',' .
							'created_at=\'' . $param['created_at']. '\'' ;
						$insert_res = $this->db->query($insert);
						//echo $insert."<br>";
					}
				}
			}
		}

		return true;
	}

	function exportCSVCategory($data){
		$this->load->helper('common_helper');

		$csv_data = [];
		$header = ['Id', 'Category (English)', 'Category (Arabic)', 'Category (English-Arabic)', 'Category (Arabic-English)',  'Sequence No', 'Created', 'Modified', 'Status'];
		array_push($csv_data, $header);

		foreach ($data as $d) {

			$id = !empty($d->id) ? $d->id : '-';
			$eng_name = !empty($d->eng_name) ? $d->eng_name : '-';
			$ar_name = !empty($d->ar_name) ? $d->ar_name : '-';
			$eng_ar_name = !empty($d->eng_ar_name) ? $d->eng_ar_name : '-';
			$ar_eng_name = !empty($d->ar_eng_name) ? $d->ar_eng_name : '-';
			$sequence_no = !empty($d->sequence_no) ? $d->sequence_no : '-';
			$created_at = !empty($d->created_at) ? $d->created_at : '-';
			$updated_at = !empty($d->updated_at) ? $d->updated_at : '-';
			$status = ($d->status == 1) ? 'Active' : 'Inactive';

			$line = [
				$id,
				$eng_name,
				$ar_name,
				$eng_ar_name,
				$ar_eng_name,
				$sequence_no,
				$created_at,
				$updated_at,
				$status
			];
			array_push($csv_data, $line);
		}

		export_to_csv($csv_data, 'category.csv');
		exit;
	}

	function exportCSVSubCategory($filter){
		$this->load->helper('common_helper');

		//status != -1 (get non deleted records)
		$this->db->where('C.status !=', -1);
		$this->db->where('SC.status !=', -1);
		$this->db->where('S.status !=', -1);

		if (!empty($filter['category'])) {
			$this->db->where('C.id', $filter['category']);
		}

		$this->db->select(
			'C.id as category_id, C.eng_name as category,
			SC.id as sub_category_id, SC.eng_name as sc_eng_name, SC.ar_name, SC.eng_ar_name, SC.ar_eng_name, SC.status as sc_status,
			S.id as sentence_id, S.en, S.ar, S.en_ar, S.ar_en, S.status as s_status'
		)
			->from('category as C')
			->join('sub_category as SC', 'C.id = SC.category_id')
			->join('sentences as S', 'SC.id = S.sub_category_id');

		$this->db->order_by('category', 'ASC');
		$this->db->order_by('sc_eng_name', 'ASC');
		$this->db->order_by('S.en', 'ASC');

		$data = $this->db->get()->result();
		//echo '<pre>'; print_r($data); exit;

		$csv_data = [];
		$header = [
			'Category ID',
			'Category',
			'Subcategory ID',
			'Subcategory (English)',
			'Subcategory (Arabic)',
			'Subcategory (English-Arabic)',
			'Subcategory (Arabic-English)',
			'Subcategory Status',
			'Sentence ID',
			'Sentence (English)',
			'Sentence (Arabic)',
			'Sentence (English-Arabic)',
			'Sentence (Arabic-English)',
			'Sentence Status'
		];
		array_push($csv_data, $header);

		if(!empty($data)) {
			foreach($data as $d) {
				$category_id = !empty($d->category_id) ? $d->category_id : '-';
				$category_name = !empty($d->category) ? $d->category : '-';
				$subcategory_id = !empty($d->sub_category_id) ? $d->sub_category_id : '-';
				$sc_eng_name = !empty($d->sc_eng_name) ? $d->sc_eng_name : '-';
				$sc_ar_name = !empty($d->ar_name) ? $d->ar_name : '-';
				$sc_eng_ar_name = !empty($d->eng_ar_name) ? $d->eng_ar_name : '-';
				$sc_ar_eng_name = !empty($d->ar_eng_name) ? $d->ar_eng_name : '-';
				$sc_status = ($d->sc_status == 1) ? 'Active' : 'Inactive';
				$sentence_id = !empty($d->sentence_id) ? $d->sentence_id : '-';
				$eng_text = !empty($d->en) ? $d->en : '-';
				$ar_text = !empty($d->ar) ? $d->ar : '-';
				$en_ar_text = !empty($d->en_ar) ? $d->en_ar : '-';
				$ar_en_text = !empty($d->ar_en) ? $d->ar_en : '-';
				$s_status = ($d->s_status == 1) ? 'Active' : 'Inactive';

				$line = [
					$category_id,
					$category_name,
					$subcategory_id,
					$sc_eng_name,
					$sc_ar_name,
					$sc_eng_ar_name,
					$sc_ar_eng_name,
					$sc_status,
					$sentence_id,
					$eng_text,
					$ar_text,
					$en_ar_text,
					$ar_en_text,
					$s_status
				];
				array_push($csv_data, $line);
			}
		}

		export_to_csv($csv_data, 'subcategory.csv');
		exit;
	}

	public function updatetable() {
		$select="SELECT id FROM sub_category";
		$sub_category_ids =$this->db->query($select);
		$sub_category_ids = $sub_category_ids->result_array();
		//echo '<pre>'; print_r($sub_category_ids); exit;
		foreach($sub_category_ids as $id) {
			$query = 'select sentence from sentences_new1 where language_type = "en-ar" and sub_category_id='.$id['id'];
			$sentences_new =$this->db->query($query);
			$sentences_new = $sentences_new->result_array();
			//echo '<pre>'; print_r($sentences_new); exit;

			$query = 'select id from sentences where sub_category_id='.$id['id'];
			$sentences =$this->db->query($query);
			$sentences = $sentences->result_array();
			//echo '<pre>'; print_r($sentences); exit;
			foreach($sentences as $i => $sid) {
				$update = 'UPDATE sentences set en_ar="'.$sentences_new[$i]['sentence']. '" where id='.$sid['id'];
				$this->db->query($update);
				//echo $update.'<br>';
			}
		}
		echo 'done';
	}

	public function get_category_wise_item_count() {
		$fields = 'C.id, C.eng_name as category, count(C.id) as cnt';
		$this->db->select($fields);
		$this->db->from('category C');
		$this->db->join('sentences S', 'C.id = S.category_id');

		$this->db->where('C.status !=', -1);
		$this->db->where('S.status !=', -1);

		$this->db->group_by('C.id');
		$order_by = ['C.eng_name' => 'ASC'];
		if(!empty($order_by)) {
			foreach($order_by as $k => $v) {
				$v = !empty($v) ? $v : 'ASC';
				$this->db->order_by($k, $v);
			}
		}
		$data = $this->db->get()->result();;

		return $data;
	}
}