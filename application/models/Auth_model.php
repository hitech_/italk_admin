<?php
class Auth_model extends CI_Model{
    function __construct() {
    	parent::__construct();
    }

    public function registeruser($param){
    	$this->db->where(array('u_email_id'=>$param['u_email_id']));
    	$this->db->from('users');
    	$resultcount=$this->db->count_all_results();
    	if($resultcount > 0){
    		return array('status'=>false, 'error'=>'User already exists in the database');
    	}
    	return $this->db->insert('users', $param);
    }

    public function auth($param){
    	$data=array();
    	$selectbyemailid="SELECT * FROM users WHERE (u_email_id='".$param['u_email_id']."' OR u_username='".$param['u_email_id']."')";
    	$res=$this->db->query($selectbyemailid);
    	$results=$res->result();
    	$resultscount=count($results);
    	if($resultscount <= 0){
    		$data['status']=false;
    		$data['data']=false;
    		$data['error']['validation_errors']['emailid']='Email id does not exist';
    		return $data;
    	}
        if($param['forgotpassword']){
            $strtoshuffle='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
            $randompass=substr(str_shuffle($strtoshuffle), 0, 8);
            $updatepassword="UPDATE users SET u_password='".md5($randompass)."' WHERE u_email_id='".$param['u_email_id']."'";
            $resupdate=$this->db->query($updatepassword);
            if($resupdate){
                $param['name']=$results[0]->u_username;
                $param['randompass']=$randompass;
                $emailtemp=$this->emailtemplate($param);
                $emailsubject="Your new password for italk";
                $emailbody=$emailtemp;
                $this->email->from('italk@example.com', 'Italk');
                $this->email->to($results[0]->u_email_id);
                $this->email->subject($emailsubject);
                $this->email->message($emailbody);
                if($this->email->send()){
                    $data['status']=true;
                    $data['data']=true;
                    $data['error']=false;
                }else{
                    $data['status']=false;
                    $data['data']=false;
                    $data['error']='Error occurred while sending email';
                }
            }else{
                $data['status']=false;
                $data['data']=false;
                $data['error']='Error occurred while updating new password';
            }
            return $data;
        }
        if(md5($param['u_password'])!=$results[0]->u_password){
    		$data['status']=false;
    		$data['data']=false;
    		$data['error']['validation_errors']['password']='Password entered is wrong';
    		return $data;
    	}else{
        if(isset($_POST['login_time']) && !empty($_POST['login_time'])){
          $this->db->set('last_login_date',$_POST['login_time']);
          $this->db->where('u_user_id',$results[0]->u_user_id);
          $this->db->update('users');
        }
        $data['status']=true;
    		$data['data']=$results[0];
    		$data['error']=false;
    		return $data;
    	}
    }

    public function check_parent_email($param){
        $select="SELECT u_user_id FROM users WHERE u_parent_email_id='".$param['u_email_id']."'";
        $res=$this->db->query($select);
        $results=$res->result_array();
        return count($results);
    }

    public function check_email($param){
        $select="SELECT u_user_id FROM users WHERE u_email_id='".$param['u_parent_email_id']."'";
        $res=$this->db->query($select);
        $results=$res->result_array();
        return count($results);
    }

    public function check_mobile_unique($param){
        $select="SELECT u_user_id FROM users WHERE u_mobileno='".$param['mobile_no']."'";
        $res=$this->db->query($select);
        $results=$res->result_array($res);
        return count($results);
    }

    public function check_email_in_both($param){
        $select="SELECT u_user_id FROM users WHERE u_email_id='".$param['emailid']."' OR u_parent_email_id='".$param['emailid']."'";
        $res=$this->db->query($select);
        $results=$res->result_array($res);
        return count($results);
    }

    public function updateprofile($param){
        $update="UPDATE users SET ";
        if(!empty($param['u_email_id'])){
            $update.="u_email_id='".$param['u_email_id']."',";
        }
        if(!empty($param['u_mobileno'])){
            $update.="u_mobileno='".$param['u_mobileno']."',";
        }
        if(!empty($param['u_landline'])){
            $update.="u_landline='".$param['u_landline']."',";
        }
        $update=rtrim($update, ',');
        $update=$update." WHERE u_user_id=".$param['u_user_id']."";
        $res=$this->db->query($update);
        if($res){
            return true;
        }else{
            return false;
        }
    }

    public function check_existing_password($param){
        $select="SELECT u_user_id FROM users WHERE u_password='".md5($param['u_password'])."' AND u_user_id=".$param['u_user_id']."";
        //echo $select; die;
        $res=$this->db->query($select);
        if($res->num_rows() > 0){
            return true;
        }
        return false;
    }

    public function changepassword($param){
        //check existing password
        $select="SELECT u_user_id FROM users WHERE u_password='".md5($param['u_existing_password'])."' AND u_user_id=".$param['u_user_id']."";
        $res=$this->db->query($select);
        if($res->num_rows() <= 0){
            return false;
        }
        $update="UPDATE users SET u_password='".md5($param['u_new_password'])."' WHERE u_user_id=".$param['u_user_id']."";
        if($this->db->query($update)){
            return true;
        }else{
            return false;
        }
    } 

    public function emailtemplate($param){
        return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>iTalk - Reset Password</title>
    <style type="text/css" rel="stylesheet" media="all">
    /* Base ------------------------------ */

    *:not(br):not(tr):not(html) {
      font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
      box-sizing: border-box;
    }

    body {
      width: 100% !important;
      height: 100%;
      margin: 0;
      line-height: 1.4;
      background-color: #F2F4F6;
      color: #74787E;
      -webkit-text-size-adjust: none;
    }

    p,
    ul,
    ol,
    blockquote {
      line-height: 1.4;
      text-align: left;
    }

    a {
      color: #26a69a;
    }

    a.email-masthead_name img {
      border: none;
      max-width: 100%;
      width: 100px
    }
    /* Layout ------------------------------ */

    .email-wrapper {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #F2F4F6;
    }

    .email-content {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    /* Masthead ----------------------- */

    .email-masthead {
      padding: 12px 0;
      text-align: center;
    }

    .email-masthead_name {
      font-size: 16px;
      font-weight: bold;
      color: #bbbfc3;
      text-decoration: none;
      text-shadow: 0 1px 0 white;
    }
    /* Body ------------------------------ */

    .email-body {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      border-top: 1px solid #EDEFF2;
      border-bottom: 1px solid #EDEFF2;
      background-color: #FFFFFF;
    }

    .email-body_inner {
      width: 600px;
      margin: 0 auto;
      padding: 0;
      -premailer-width: 600px;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #FFFFFF;
    }

    .email-footer {
      width: 600px;
      margin: 0 auto;
      padding: 0;
      -premailer-width: 600px;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      text-align: center;
    }

    .email-footer p {
      color: #AEAEAE;
    }

    .body-action {
      width: 100%;
      margin: 30px auto;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      text-align: center;
    }

    .body-sub {
      margin-top: 25px;
      padding-top: 25px;
      border-top: 1px solid #EDEFF2;
    }

    .content-cell {
      padding: 25px 20px;
    }


    @media only screen and (max-width: 600px) {
      .email-body_inner,
      .email-footer {
        width: 100% !important;
      }
    }

    @media only screen and (max-width: 500px) {
      .button {
        width: 100% !important;
      }
    }
    /* Buttons ------------------------------ */

    .button {
      background-color: #26a69a;
      border-top: 10px solid #26a69a;
      border-right: 18px solid #26a69a;
      border-bottom: 10px solid #26a69a;
      border-left: 18px solid #26a69a;
      display: inline-block;
      color: #FFF;
      text-decoration: none;
      -webkit-text-size-adjust: none;
    }

    .button--green {
      background-color: #26a69a;
      border-top: 10px solid #26a69a;
      border-right: 18px solid #26a69a;
      border-bottom: 10px solid #26a69a;
      border-left: 18px solid #26a69a;
    }

    h1 {
      margin-top: 0;
      color: #2F3133;
      font-size: 19px;
      font-weight: bold;
      text-align: left;
    }

    h2 {
      margin-top: 0;
      color: #2F3133;
      font-size: 16px;
      font-weight: bold;
      text-align: left;
    }

    h3 {
      margin-top: 0;
      color: #2F3133;
      font-size: 14px;
      font-weight: bold;
      text-align: left;
    }

    p {
      margin-top: 0;
      color: #74787E;
      font-size: 16px;
      line-height: 1.5em;
      text-align: left;
    }

    p.sub {
      font-size: 12px;
    }

    p.center {
      text-align: center;
    }
    </style>
  </head>
  <body>
    <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center">
          <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td class="email-masthead" style="line-height:0;">
                <a href="javascript:;" class="email-masthead_name"><img src="http://www.azeemansari.me/iTalk/images/logo-text.png" alt="iTalk"></a>
              </td>
            </tr>
            <!-- Email Body -->
            <tr>
              <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
                <table class="email-body_inner" align="center" width="600" cellpadding="0" cellspacing="0">
                  <!-- Body content -->
                  <tr>
                    <td class="content-cell">
                      <h1>Hi '.$param['name'].',</h1>
                      <p>You recently requested to reset your password for your <strong>iTalk</strong> account. Use the default password below to reset it.</p>
                      <!-- Action -->
                      <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td align="center">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td align="left">
                                  <table border="0" width="80%" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td>
                                        <span class="button button--green" style="text-align:center;width:100%;font-size:1.5em;">'.$param['randompass'].'</span>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <p>If you did not request a password reset, please ignore this email or <a href="azim.frontend@gmail.com">contact support</a> if you have questions.</p>
                      <p>Thanks,
                        <br>The iTalk Team</p>
                      <!-- Sub copy -->
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table class="email-footer" align="center" width="600" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="content-cell" align="center">
                      <p style="text-align:center"><a href="javascript:;"><img src="http://www.azeemansari.me/iTalk/images/hi-tech-logo.png" style="width:200px;" alt="Hi-Techpeople"></a></p>
                      <p class="sub align-center" style="margin-bottom:0;text-align:center;">&copy; 2018 iTalk. All rights reserved.</p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>
';
    }
}