<?php

class Services_model extends CI_Model {
    function __construct() {
    	parent::__construct();
    }

    public function get_categories($limit, $offset){
    	$this->get_where_conditions();
        $this->db->select('id,ar_name,eng_name,eng_ar_name,ar_eng_name,image,ar_audio,sequence_no,status,track_id');
        $this->db->limit($limit, $offset);
    	return $this->db->get('category')->result_array();
    }

    public function get_total_categories() {
        $this->get_where_conditions();
        $this->db->from('category');
        $count = $this->db->count_all_results();
        return $count;
    }

    public function get_subcategories($limit, $offset) {
    	$this->get_where_conditions();
        $this->db->select('id,ar_name,eng_name,eng_ar_name,ar_eng_name,sc_image,sc_ar_audio,category_id,sequence_no,status,track_id');
        $this->db->limit($limit, $offset);
    	return $this->db->get('sub_category')->result_array();
    }

    public function get_total_subcategories() {
        $this->get_where_conditions();
        $this->db->from('sub_category');
        $count = $this->db->count_all_results();

        return $count;
    }

    public function get_sentences($limit, $offset){
        $this->get_where_conditions();
        $this->db->select('id,category_id,sub_category_id,en,ar,en_ar,ar_en,ar_audio,status,track_id');
        $this->db->limit($limit, $offset);
        return $this->db->get('sentences')->result_array();
    }

    public function get_total_sentences() {
        $this->get_where_conditions();
        $this->db->from('sentences');
        $count = $this->db->count_all_results();

        return $count;
    }

    /*
    public function get_deleted_list() {
        $this->where_conditions();
        return $this->db->get('deleted_list')->result_array();
    }
    */

    public function get_words(){
    	$this->where_conditions();
    	return $this->db->get('words')->result_array();
    }

    private function where_conditions(){
    	$date = $this->date;
    	if(!empty($date)){
    		$this->db->where('updated_at >',$this->date);
    	}
    	$this->db->order_by('updated_at','ASC');
    	//$this->db->order_by('id','DESC');
    }

    private function get_where_conditions(){
    	$track_id = $this->track_id;
    	if(!empty($track_id)){
    		$this->db->where('track_id >',$this->track_id);
    	} else {
            $this->db->where('status',1);
        }
    	$this->db->order_by('track_id','ASC');
    }

    public function addlog($param){
        $insert="INSERT INTO logs SET category_id=".$param['category_id'].", sub_category_id=".$param['sub_category_id'].", senetence_id=".$param['senetence_id'].", user_id=".$param['user_id'].", mobile_created_at=".$param['mobile_created_at'].", created_at=".date('Y-m-d H:i:s');
        //echo $insert; die;
        if($this->db->query($insert)){
            return true;
        }else{
            return false;
        }
    }
}