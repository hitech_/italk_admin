<?php
class Do_common extends CI_Model {
    function __construct() {
    	parent::__construct();
    }

    public function delete($param){
		$delete = "UPDATE ".$param['table_name']." SET status=-1, updated_at ='".$param['datetime']."' WHERE id=".$param['id'];
		$res = $this->db->query($delete);
		if($res){
			return true;
		}
		return false;

		/*
		$prefox=($param['prefix']) ? $param['prefix'] : '';
		$delete="DELETE FROM ".$param['module']." WHERE ".$param['prefix']."id=".$param['id'];
		$res=$this->db->query($delete);
		if($res){
			if($param['module']=='category'){
				$dl_element='1';
			}else if($param['module']=='sub_category'){
				$dl_element='2';
			}
			
			$insert="INSERT INTO deleted_list SET deleted_id=".$param['id'].", deleted_type='".$dl_element."', created_at='".$param['created_date']."', updated_at='".$param['created_date']."'";
			$resinsert=$this->db->query($insert);
			if($resinsert){
				return true;
			}else{
				return false;
			}
		}
		return false;
		*/
	}

	public function delete_audio($param){
		$delete = "UPDATE ".$param['table_name']." SET ".$param['column_name']."='', updated_at ='".$param['datetime']."' WHERE id=".$param['id'];
		$res = $this->db->query($delete);
		if($res){
			return true;
		}
		return false;
	}

	public function toggle_status($param){
		$query = "UPDATE ".$param['table_name']." SET ".$param['column_name']."=". $param['status'] . ", updated_at ='".$param['datetime']."' WHERE id=".$param['id'];
		$res = $this->db->query($query);
		if($res){
			return true;
		}
		return false;
	}
}