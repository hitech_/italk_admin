<?php
Class Do_logs extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	public function logs($param, $limit, $offset, $order_by){

		if($param && $param['disabilitytype']){
			switch($param['disabilitytype']){
				case 1:
					$disabilitytype='Normal';
					break;
				case 2:
					$disabilitytype='Autism';
					break;
				case 3:
					$disabilitytype='Blind';
					break;
				case 4:
					$disabilitytype='Handicap';
					break;
				default:
					$disabilitytype='Normal';
			}
			$this->db->where("U.u_disability_type", $disabilitytype);
		}

		if($param && $param['duration']){

			switch($param['duration']){
				case 1://daily
					$dateobj=new DateTime();
					$durationfrom = date('Y-m-d 00:00:00');
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;

				case 2://weekly
					$day = date('w');
					$dateobj = new DateTime();
					$first_day_of_week = $dateobj->modify('-'.$day.' days');
					$durationfrom = $first_day_of_week->format('Y-m-d 00:00:00');

					$dateobj = new DateTime();
					$last_day_of_week = $dateobj->modify('+'.(6-$day).' days');
					$durationto = $last_day_of_week->format('Y-m-d 23:59:59');

					break;

				case 3:// monthly

					$dateobj = new DateTime();
					$durationfrom = $dateobj->format('Y-m-01 00:00:00');

					$dateobj = new DateTime();
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;
				case 4:
					$dateobj = new DateTime($param['fromdate']);
					$durationfrom = $dateobj->format('Y-m-d 00:00:00');

					$dateobj = new DateTime($param['todate']);
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;

				default:
					$dateobj = new DateTime();
					$durationfrom = date('Y-m-d 00:00:00');
					$durationto = $dateobj->format('Y-m-d 23:59:59');
			}

			$this->db->where("L.created_at BETWEEN '".$durationfrom."' AND '".$durationto."'");
		}

		$fields = 'U.u_user_id, U.u_username, U.u_disability_type, U.last_login_date, U.last_logout_time';
		$this->db->select($fields)
			->from('logs as L')
			->join('users as U', 'L.user_id = U.u_user_id');

		$this->db->group_by('L.user_id');
		if(!empty($order_by)) {
			foreach($order_by as $k => $v) {
				$v = !empty($v) ? $v : 'ASC';
				$this->db->order_by($k, $v);
			}
		}
		$this->db->limit($limit, $offset);

		$data = $this->db->get()->result();

		//echo '<pre>'; print_r($data); exit;

		return $data;
	}

	public function get_total_logs($param) {
		if($param && $param['disabilitytype']){
			switch($param['disabilitytype']){
				case 1:
					$disabilitytype='Normal';
					break;
				case 2:
					$disabilitytype='Autism';
					break;
				case 3:
					$disabilitytype='Blind';
					break;
				case 4:
					$disabilitytype='Handicap';
					break;
				default:
					$disabilitytype='Normal';
			}
			$this->db->where("U.u_disability_type", $disabilitytype);
		}

		if($param && $param['duration']){

			switch($param['duration']){
				case 1://daily
					$dateobj=new DateTime();
					$durationfrom = date('Y-m-d 00:00:00');
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;

				case 2://weekly
					$day = date('w');
					$dateobj = new DateTime();
					$first_day_of_week = $dateobj->modify('-'.$day.' days');
					$durationfrom = $first_day_of_week->format('Y-m-d 00:00:00');

					$dateobj = new DateTime();
					$last_day_of_week = $dateobj->modify('+'.(6-$day).' days');
					$durationto = $last_day_of_week->format('Y-m-d 23:59:59');

					break;

				case 3:// monthly

					$dateobj = new DateTime();
					$durationfrom = $dateobj->format('Y-m-01 00:00:00');

					$dateobj = new DateTime();
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;
				case 4:
					$dateobj = new DateTime($param['fromdate']);
					$durationfrom = $dateobj->format('Y-m-d 00:00:00');

					$dateobj = new DateTime($param['todate']);
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;

				default:
					$dateobj = new DateTime();
					$durationfrom = date('Y-m-d 00:00:00');
					$durationto = $dateobj->format('Y-m-d 23:59:59');
			}

			$this->db->where("L.created_at BETWEEN '".$durationfrom."' AND '".$durationto."'");
		}

		$this->db->group_by('L.user_id');
		$this->db->from('logs as L')
			->join('users as U', 'L.user_id = U.u_user_id');
		$count = $this->db->count_all_results();
		return $count;
	}

	public function get_userlogs($param, $limit, $offset, $order_by){
//		$this->output->enable_profiler(TRUE);
		$this->db->where('l.user_id',$param['user_id']);

		if($param && $param['duration']){
			switch($param['duration']){
				case 1://daily
					$dateobj=new DateTime();
					$durationfrom = date('Y-m-d 00:00:00');
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;

				case 2://weekly
					$day = date('w');
					$dateobj = new DateTime();
					$first_day_of_week = $dateobj->modify('-'.$day.' days');
					$durationfrom = $first_day_of_week->format('Y-m-d 00:00:00');

					$dateobj = new DateTime();
					$last_day_of_week = $dateobj->modify('+'.(6-$day).' days');
					$durationto = $last_day_of_week->format('Y-m-d 23:59:59');

					break;

				case 3:// monthly

					$dateobj = new DateTime();
					$durationfrom = $dateobj->format('Y-m-01 00:00:00');

					$dateobj = new DateTime();
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;
				case 4:
					$dateobj = new DateTime($param['fromdate']);
					$durationfrom = $dateobj->format('Y-m-d 00:00:00');

					$dateobj = new DateTime($param['todate']);
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;

				default:
					$dateobj = new DateTime();
					$durationfrom = date('Y-m-d 00:00:00');
					$durationto = $dateobj->format('Y-m-d 23:59:59');
			}

			$this->db->where("l.created_at BETWEEN '".$durationfrom."' AND '".$durationto."'");
		}

		$fields = 'l.success, l.created_at, l.lang_type, c.eng_name c_name, c.ar_name c_name_ar, s.eng_name s_name, s.ar_name s_name_ar, sen.en as sentence, sen.ar as sentence_ar';
		$this->db->select($fields);
		$this->db->from('logs l');
		$this->db->join('category c','c.id = l.category_id');
		$this->db->join('sub_category s','s.id = l.sub_category_id');
		$this->db->join('sentences sen','sen.id = l.sentence_id');

		if(!empty($order_by)) {
			foreach($order_by as $k => $v) {
				$v = !empty($v) ? $v : 'ASC';
				$this->db->order_by($k, $v);
			}
		}
		$this->db->limit($limit, $offset);
		$data = $this->db->get()->result();
		//echo $this->db->last_query();
		//echo '<pre>';print_r($data); exit;

	    return $data;
	}

	public function get_total_userlogs($param) {
		$this->db->where('l.user_id',$param['user_id']);

		if($param && $param['duration']){
			switch($param['duration']){
				case 1://daily
					$dateobj=new DateTime();
					$durationfrom = date('Y-m-d 00:00:00');
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;

				case 2://weekly
					$day = date('w');
					$dateobj = new DateTime();
					$first_day_of_week = $dateobj->modify('-'.$day.' days');
					$durationfrom = $first_day_of_week->format('Y-m-d 00:00:00');

					$dateobj = new DateTime();
					$last_day_of_week = $dateobj->modify('+'.(6-$day).' days');
					$durationto = $last_day_of_week->format('Y-m-d 23:59:59');

					break;

				case 3:// monthly

					$dateobj = new DateTime();
					$durationfrom = $dateobj->format('Y-m-01 00:00:00');

					$dateobj = new DateTime();
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;
				case 4:
					$dateobj = new DateTime($param['fromdate']);
					$durationfrom = $dateobj->format('Y-m-d 00:00:00');

					$dateobj = new DateTime($param['todate']);
					$durationto = $dateobj->format('Y-m-d 23:59:59');

					break;

				default:
					$dateobj = new DateTime();
					$durationfrom = date('Y-m-d 00:00:00');
					$durationto = $dateobj->format('Y-m-d 23:59:59');
			}

			$this->db->where("l.created_at BETWEEN '".$durationfrom."' AND '".$durationto."'");
		}

		$this->db->from('logs l');
		$count = $this->db->count_all_results();
		return $count;
	}

	public function get_user_detail($where=[], $order_by = []) {
		if(!empty($where['user_id'])) {
			$this->db->where('u_user_id', $where['user_id']);
		}

		$fields = 'u_user_id, u_username, u_parent_email_id, u_email_id';
		$this->db->select($fields);
		$this->db->from('users');

		if(!empty($order_by)) {
			foreach($order_by as $k => $v) {
				$v = !empty($v) ? $v : 'ASC';
				$this->db->order_by($k, $v);
			}
		}
		$data = $this->db->get()->result();
		//echo '<pre>';print_r($data); exit;

		return $data;
	}

	public function get_reports($user_id) {

		$fields = 'category_id, SUM(success) as success_cnt';
		$this->db->select($fields);
		$this->db->from('logs');

		$this->db->where('user_id', $user_id);

		$this->db->group_by('category_id');

		$data = $this->db->get()->result();

		return $data;
	}

	function exportCSVReportCard($data){
		$this->load->helper('common_helper');

		$csv_data = [];
		$header = ['', 'Report Card', 'Date:', date('d-m-Y')];
		array_push($csv_data, $header);

		$user_name = !empty($data['users'][0]->u_username) ? ucwords(strtolower($data['users'][0]->u_username)) : '-';
		$header = ['User Name', $user_name];
		array_push($csv_data, $header);

		array_push($csv_data, []);

		$header = ['Category', 'Result'];
		array_push($csv_data, $header);

		if(!empty($data['category_wise_cnt'])) {
			foreach ($data['category_wise_cnt'] as $key => $val) {

				$category = !empty($val->category) ? ucwords(strtolower($val->category)) : '-';
				$category_id = !empty($val->id) ? $val->id : 0;
				$category_cnt = !empty($val->cnt) ? $val->cnt : '-';
				$success_cnt = !empty($data['log_report'][$category_id]->success_cnt) ? $data['log_report'][$category_id]->success_cnt : 0;

				$line = [
					$category,
					$success_cnt . ' / ' . $category_cnt,
				];
				array_push($csv_data, $line);
			}
		}
		$report_name = str_replace(' ', '_', strtolower($user_name));
		export_to_csv($csv_data, 'reportcard_'.$report_name.'.csv');
		exit;
	}
}
?>