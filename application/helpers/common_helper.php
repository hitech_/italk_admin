<?php defined('BASEPATH') OR exit('No direct script access allowed.');

if (!function_exists('get_client_ip')) {
    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = '';
        return $ipaddress;
    }
}

if(!function_exists('uploadImage')){
    function uploadImage($folder_name,$files)
    {   
        $ci = &get_instance();
        $filename=array();
        if (!empty($files['name'])) {
            $uplaodFileName = date("YmdHis")."_".RenameUploadFile($files['name']);
            $_FILES['userfile']['name']= $uplaodFileName;
            $_FILES['userfile']['type']= $files['type'];
            $_FILES['userfile']['tmp_name']= $files['tmp_name'];
            $_FILES['userfile']['error']= $files['error'];
            $_FILES['userfile']['size']= $files['size'];
            $ci->upload->initialize(set_upload_options($folder_name));
            $upload=$ci->upload->do_upload();
            if($upload)
            {
                return $uplaodFileName;
            }else{
                echo "<pre>"; print_r($ci->upload->display_errors()); echo "</pre>";
            }
        }  
    }
}



function SaveMedia($model,$file,$upload_type='')
{
    $filename="";
    CreateFolderbyname($model);
    
                $file=$_FILES['la'];
                $uplaodFileName = RenameUploadFile($_FILES['la']['name']['key1_img_1']);
                $_FILES['userfile']['name']= date("YmdHis")."_".$uplaodFileName;
                $_FILES['userfile']['type']= $file['type']['key1_img_1'];
                $_FILES['userfile']['tmp_name']= $file['tmp_name']['key1_img_1'];
                $_FILES['userfile']['error']= $file['error']['key1_img_1'];
                $_FILES['userfile']['size']= $file['size'][['key1_img_1']];
                $this->upload->initialize(set_upload_options('life_at_axis'));
                $upload=$this->upload->do_upload();
                if($upload)
                {
                    echo "File Uploaded";
                }else{
                    echo "<pre>"; print_r($this->upload->display_errors());
                    echo "</pre>";die;
                }
            // $uplaodFileName = RenameUploadFile($file['name']);
            // $_FILES['userfile']['name']= date("YmdHis")."_".$uplaodFileName;
            // $_FILES['userfile']['type']= $file['type'][$key];
            // $_FILES['userfile']['tmp_name']= $file['tmp_name'][$key];
            // $_FILES['userfile']['error']= $file['error'][$key];
            // $_FILES['userfile']['size']= $file['size'][$key];
            // $this->upload->initialize(set_upload_options($model));
            // $upload=$this->upload->do_upload();
            // if($upload)
            // {
            //     return $uplaodFileName;
            // }else{
            //     echo "<pre>"; print_r($this->upload->display_errors());
            //     echo "</pre>";die;
            // }
}


if(!function_exists('CreateFolderbyname')){
    function CreateFolderbyname($folderName)
    {
        $targetPath=dirname(dirname(dirname(__FILE__)))."/uploads/".$folderName."/";
        if(!is_dir($targetPath))
        {
            mkdir($targetPath, 0777, true);     
        }
    }
}

function RenameUploadFile($data) {
    $search = array("'","  "," ","(",")","&","-","\"","\\","?",":","/");
    $replace = array("","_","_","","","","","","","","","","");
    $new_data=str_replace($search, $replace, $data);
    return strtolower($new_data);
}

function set_upload_options($path)
{   
    $config = array();
    $config['upload_path'] = './uploads/'.$path.'/';
    $config['allowed_types'] = '*';
    $config['max_size']      = '0';
    $config['overwrite']     = TRUE;
    return $config;
}
if (!function_exists('query_record')) {

    function query_record($record, $params) {
        $data = array();
        foreach ($record['list'] as $key => $value) {
            $i = 0;
            foreach ($value as $v) {
                $data[$key][$i] = $v;
                $i++;
            }
        }
        $json_data = array(
            "draw" => intval($params['draw']),
            "recordsTotal" => intval($record['totalRecords']),
            "recordsFiltered" => intval($record['totalRecords']),
            "data" => $data
        );
        return json_encode($json_data);
    }
}

if (!function_exists('get_option_type')) {
    function get_option_type() {
       $ans_type=array(
            'textbox' => 'Subjective',
            'radio' => 'Single choice',
            'checkbox' => 'Multiple Choice'
        );
       return $ans_type;
    }
}
if (!function_exists('get_social_media')) {
    function get_social_media() {
       $social_media=array(
            'twitter' => 'Twitter',
            'linkedin' => 'LinkedIn',
            /*'facebook' => 'Facebook',
            'pinterest' => 'Pinterest',
            'youtube' => 'Youtube'*/
        );
       return $social_media;
    }
}
if (!function_exists('get_seo_tag')) {
    function get_seo_tag($type='') {
        $ci = &get_instance();
        $seo_tag= array(
            'title' =>'LIGHTBOX',
            'description' => 'We&#8217;re focussed on building global consumer tech companies from India&#8217;s new economy. Have a look at the companies in our portfolio to know more about how we operate venture. Some are companies we have built in the past, most are companies we are building for the future, and all have been a product of an &hellip;',
            'keywords' =>'',
        );
        if($type==''){
           $result = $seo_tag;
        }else{
            $res =$ci->db->get_where("page_seo",array("pagename"=>$type))->row_array();
            if(sizeof($res)>0)
                $result = json_decode($res['seo_tag'],true);
            else
                $result = $seo_tag;
        }
        return $result;
    }
}
if (!function_exists('getSlug')) {
    function getSlug($text) {
        // replace non letter or digits by -
          $text = preg_replace('~[^\pL\d]+~u', '-', $text);
          // transliterate
          $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
          // remove unwanted characters
          $text = preg_replace('~[^-\w]+~', '', $text);
          // trim
          $text = trim($text, '-');
          // remove duplicate -
          $text = preg_replace('~-+~', '-', $text);

          // lowercase
          $text = strtolower($text);
          if (empty($text)) {
            return 'n-a';
          }
        return $text;
    }
}
if (!function_exists('get_footer_icon')) {
    function get_footer_icon($key) {
       $icon=array(
            'twitter' => '&#xf099;',
            'linkedin' => '&#xf0e1;',
            'facebook' => '&#xe802',
            'instagram' => '&#xf16d;',
            'google' => '&#xe800;',
            'pinterest' => '&#xf0d3;'
        );
       return $icon[$key];
    }
}

if (!function_exists('export_to_csv')) {
    function export_to_csv($data, $filename='') {
        $filename = !empty($filename) ? $filename : 'report.csv';
        ob_clean();
        //headers
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Description: File Transfer');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Content-Transfer-Encoding: binary');

        // file creation
        $file = fopen('php://output', 'w');

        //add BOM to fix UTF-8 in Excel
        fputs($file, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

        if ($file) {
            foreach ($data as $d) {
                fputcsv($file, $d);
            }
            fclose($file);
        }
        exit;
    }
}
?>
