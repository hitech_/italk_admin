<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * MY_Controller Class
 *
 *
 * @package Project Name
 * @subpackage  Controllers
 */
class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

}

class Authorization_controller extends CI_Controller {

    private $secret_key = '';
    private $auth_token = '';
    private $__loggedIn = false;
    private $__response = array(
        'status' => false,
        'result' => array(),
        'error' => 'User not logged in..',
    );

    public function __construct() {
        parent::__construct();

        $this->load->library('JWT');
        $this->load->config('jwt', true);
        $this->secret_key = $this->config->item('encryption_key_secret', 'jwt');

        $this->getHeaders();
    }

    private function getHeaders() {
        $headers = apache_request_headers();
        if (isset($headers['Authorization'])) {
            $this->auth_token = $headers['Authorization'];
        }

        $this->check_token();
    }

    private function check_token() {
        if ($this->auth_token != '') {
            try {
                $token = JWT::decode($this->auth_token, $this->secret_key);
            } catch (Exception $e) {
                $this->__loggedIn = false;
                $this->__response['status'] = false;
                $this->__response['error'] = 'invalid token received....';
                echo $this->getResponse();
                die();
            }

            if (isset($token->email) && $this->verifyUserToken($token->email)) {
                $this->__loggedIn = true;
                $this->__response['status'] = true;
            } else {
                $this->__response['status'] = false;
                $this->__response['error'] = 'invalid token received....';
                echo $this->getResponse();
                die();
            }
        }
    }

    private function verifyUserToken($email) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email_id', $email);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function isUserLoggedIn() {
        return $this->__loggedIn;
    }

    public function create_token($user) {

        $email = $user['email_id'];

        $token = array(
            'email' => $email,
        );

        return JWT::encode($token, $this->secret_key);
    }

    public function setResponse($param) {
        if (isset($param['result'])) {
            $this->__response['result'] = $param['result'];
        }
        if (isset($param['status'])) {
            $this->__response['status'] = $param['status'];
        }
        if (isset($param['error'])) {
            $this->__response['error'] = $param['error'];
        }
    }

    public function getResponse() {
        echo json_encode($this->__response);
    }

}
